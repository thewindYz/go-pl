// 打印标准输入中出现多次的行，以该行出现的次数为开头
package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	fmt.Printf("%%")
	counts := make(map[string]int)
	input := bufio.NewScanner(os.Stdin)

	for input.Scan() {
		counts[input.Text()]++
	}

	for line, n := range counts {
		if n > 1 {
			fmt.Printf("%d\t %s\n", n, line)
		}
	}
}
