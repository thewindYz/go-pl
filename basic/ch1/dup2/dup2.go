// 从多个文件中读入字符串，并且打印出有重复行的行信息，以重复的次数为打印的开始
// 如果参数没有文件名， 则直接读取标准输入

package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {

	//test

	counts := make(map[string]int)

	fmt.Printf("the counts type is %T\n", counts)
	fmt.Printf("init map address is %p\n", &counts)
	files := os.Args[1:]

	if len(files) == 0 {
		countLines(os.Stdin, counts)
	} else {

		for _, arg := range files {
			f, err := os.Open(arg)
			if err != nil {
				fmt.Fprintf(os.Stderr, "dup2: %v\n", err)
				continue
			}
			countLines(f, counts)
			f.Close()
		}
	}

	// for line, n := range counts {
	// 	if n > 1 {
	// 		fmt.Printf("%d\t%s\n", n, line)
	// 	}
	// }

}

func countLines(f *os.File, counts map[string]int) {
	fmt.Printf("the map address update to %p\n", &counts)
	input := bufio.NewScanner(f)

	for input.Scan() {
		counts[input.Text()]++
	}
}
