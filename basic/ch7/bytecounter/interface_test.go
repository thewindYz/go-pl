package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"testing"
)

func TestInterface(t *testing.T) {
	var w io.Writer
	w = os.Stdout
	t.Log(w)
	// var v interface{}
	// t.Log(v)
	// fmt.Println("--test--")
	// fmt.Printf("%t", w)
}

type IntSet struct { /* ... */
	words []uint64
}

func (s *IntSet) String() string {
	var buf bytes.Buffer
	buf.WriteByte('{')
	for i, word := range s.words {
		if word == 0 {
			continue
		}
		for j := 0; j < 64; j++ {
			if word&(1<<uint(j)) != 0 {
				if buf.Len() > len("{") {
					buf.WriteByte(' ')
				}
				fmt.Fprintf(&buf, "%d", 64*i+j)
			}
		}
	}
	buf.WriteByte('}')
	return buf.String()
}

func TestIOWriter(t *testing.T) {

	var s IntSet
	var ss fmt.Stringer = &s
	fmt.Print(ss)
	// var w io.Writer
	// w = time.Second
}
