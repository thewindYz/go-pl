package main

import "fmt"

func main() {
	F1()
	F2()
	fmt.Println("---end--")
}

func F1() {
	fmt.Println("--end--")
}

func F2() {
	fmt.Println("--end--")
}

func F3() {
	fmt.Println("--end--")
}
