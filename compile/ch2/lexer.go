package main

import "fmt"

type State int

const (
	Initial State = iota
	If
	IDIf1
	IDIf2
	Else
	IDElse1
	IDElse2
	IDElse3
	IDElse4
	Int
	IDInt1
	IDInt2
	IDInt3
	ID
	GT
	GE
	Assignment
	Plus
	Minus
	Star
	Slash
	SemiColon
	LeftParen
	RightParen
	IntLiteral
)

type TokenType int

const (
	TPlus TokenType = iota
	TMinus
	TStar
	TSlash
	TGE
	TGT
	TEQ
	TLE
	TLT
	TSemiColon
	TLeftParen
	TRightParen
	TAssignment
	TIf
	TElse
	TInt
	TIdentifier
	TIntLiteral
	TStringLiteral
)

func (tt TokenType) String() string {
	return [...]string{
		"TPlus",
		"TMinus",
		"TStar",
		"TSlash",
		"TGE",
		"TGT",
		"TEQ",
		"TLE",
		"TLT",
		"TSemiColon",
		"TLeftParen",
		"TRightParen",
		"TAssignment",
		"TIf",
		"TElse",
		"TInt",
		"TIdentifier",
		"TIntLiteral",
		"TStringLiteral",
	}[tt]
}

type Token interface {
	getType() TokenType
	getText() string
}

type TokenReader interface {
	read() Token
	peek() Token
	unread()
	getPosition() int
	setPosition(position int)
}

type SimpleTokenReader struct {
	tokens []Token
	pos    int
}

func (s *SimpleTokenReader) read() Token {
	if s.pos < len(s.tokens) {
		token := s.tokens[s.pos]
		s.pos = s.pos + 1
		return token
	}
	return nil
}

func (s *SimpleTokenReader) peek() Token {
	if s.pos < len(s.tokens) {
		return s.tokens[s.pos]
	}
	return nil
}

func (s *SimpleTokenReader) unread() {
	if s.pos > 0 {
		s.pos = s.pos - 1
	}
}

func (s *SimpleTokenReader) getPosition() int {
	return s.pos
}

func (s *SimpleTokenReader) setPosition(position int) {
	s.pos = position
}

type SimpleToken struct {
	ttype TokenType
	text  string
}

func (s *SimpleToken) getType() TokenType {
	return s.ttype
}

func (s *SimpleToken) getText() string {
	return s.text
}

var tokens []Token
var tokenText []rune
var token *SimpleToken = &SimpleToken{
	ttype: -1,
	text:  "",
}

func isAlpha(r rune) bool {
	return r >= 'a' && r <= 'z' || r >= 'A' && r <= 'Z'
}

func isDigit(r rune) bool {
	return r >= '0' && r <= '9'
}

func isBlank(r rune) bool {
	return r == ' ' || r == '\t' || r == '\n'
}

func initToken(r rune) State {
	if len(tokenText) > 0 {
		token.text = string(tokenText)
		tokens = append(tokens, token)
		tokenText = []rune{}
		token = new(SimpleToken)
	}
	var newState State = Initial

	if isAlpha(r) { // 第一个字符是字母
		if r == 'i' {
			newState = IDInt1
		} else {
			newState = ID // 进入Id状态
		}
		token.ttype = TIdentifier
		tokenText = append(tokenText, r)
	} else if isDigit(r) {
		newState = IntLiteral
		token.ttype = TIntLiteral
		tokenText = append(tokenText, r)
	} else if r == '>' {
		newState = GT
		token.ttype = TGT
		tokenText = append(tokenText, r)
	} else if r == '+' {
		newState = Plus
		token.ttype = TPlus
		tokenText = append(tokenText, r)
	} else if r == '-' {
		newState = Minus
		token.ttype = TMinus
		tokenText = append(tokenText, r)
	} else if r == '*' {
		newState = Star
		token.ttype = TStar
		tokenText = append(tokenText, r)
	} else if r == '/' {
		newState = Slash
		token.ttype = TSlash
		tokenText = append(tokenText, r)
	} else if r == ';' {
		newState = SemiColon
		token.ttype = TSemiColon
		tokenText = append(tokenText, r)
	} else if r == '(' {
		newState = LeftParen
		token.ttype = TLeftParen
		tokenText = append(tokenText, r)
	} else if r == ')' {
		newState = RightParen
		token.ttype = TRightParen
		tokenText = append(tokenText, r)
	} else if r == '=' {
		newState = Assignment
		token.ttype = TAssignment
		tokenText = append(tokenText, r)
	} else {
		newState = Initial
	}

	return newState

}

func tokenize(code string) SimpleTokenReader {
	tokens = []Token{}

	runes := []rune(code)

	tokenText = []rune{}

	var state State = Initial

	for _, r := range runes {
		switch state {
		case Initial:
			state = initToken(r)
		case ID:
			if isAlpha(r) || isDigit(r) {
				tokenText = append(tokenText, r) // 保持标识符状态
			} else {
				state = initToken(r) // 退出标识符状态，并保存Token
			}
		case GT:
			if r == '=' {
				state = GE
				token.ttype = TGE
				tokenText = append(tokenText, r)
			}
		case GE, Assignment, Plus, Minus, Star, Slash, SemiColon, LeftParen, RightParen:
			state = initToken(r)
		case IntLiteral:
			if isDigit(r) {
				tokenText = append(tokenText, r)
			} else {
				state = initToken(r)

			}

		case IDInt1:
			if r == 'n' {
				state = IDInt2
				tokenText = append(tokenText, r)
			} else if isDigit(r) || isAlpha(r) {
				state = ID // 切换回Id状态
				tokenText = append(tokenText, r)
			} else {
				state = initToken(r)
			}

		case IDInt2:
			if r == 't' {
				state = IDInt3
				tokenText = append(tokenText, r)
			} else if isDigit(r) || isAlpha(r) {
				state = ID // 切换回id状态
				tokenText = append(tokenText, r)
			} else {
				state = initToken(r)
			}

		case IDInt3:
			if isBlank(r) {
				token.ttype = TInt
				state = initToken(r)
			} else {
				state = ID // 切换回Id状态
				tokenText = append(tokenText, r)
			}
		default:

		}

	}

	// 把最后一个token送进去
	if len(tokenText) > 0 {
		initToken(runes[len(runes)-1])
	}

	return SimpleTokenReader{
		tokens: tokens,
	}
}

func (s *SimpleTokenReader) dump() {
	fmt.Println("text type")
	var token Token
	token = s.read()
	for token != nil {
		fmt.Printf("%v  %v\n", token.getText(), token.getType())
		token = s.read()
	}
}

func main() {
	script := "int age = 45;"
	fmt.Println("parse :" + script)
	tokenReader := tokenize(script)
	tokenReader.dump()

	// 	// 测试inta的解析
	// 	script = "inta age = 45;";
	// 	System.out.println("\nparse :" + script);
	// 	tokenReader = lexer.tokenize(script);
	// 	dump(tokenReader);

	// 	// 测试in的解析
	// 	script = "in age = 45;";
	// 	System.out.println("\nparse :" + script);
	// 	tokenReader = lexer.tokenize(script);
	// 	dump(tokenReader);

	// 	// 测试>=的解析
	// 	script = "age >= 45;";
	// 	System.out.println("\nparse :" + script);
	// 	tokenReader = lexer.tokenize(script);
	// 	dump(tokenReader);

	// 	// 测试>的解析
	// 	script = "age > 45;";
	// 	System.out.println("\nparse :" + script);
	// 	tokenReader = lexer.tokenize(script);
	// 	dump(tokenReader);
}
