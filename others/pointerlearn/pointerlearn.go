package main

import "fmt"

func main() {
	size := new(int)
	fmt.Printf("size value is %d, size type is %T, size address is %v\n", *size, size, size)
	*size = 85
	fmt.Println("new size value is ", *size)
}

/*
func main() {
	a := 25
	var b *int
	if b == nil {
		fmt.Println("b is ", b)
	}
	b = &a
	fmt.Println("b is ", b)
}
*/

/*
func main() {
	a := 1
	var b *int = &a
	fmt.Printf("b type is %T\n", b)
	fmt.Println("a address is ", b)
}
*/
