package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func main() {
	s := basicAuth()
	fmt.Println(s)
}

func basicAuth() string {
	url := "http://es-cn-6ja1wurj3003ofbli.public.elasticsearch.aliyuncs.com:9200"
	username := "elastic"
	password := ""
	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	req.SetBasicAuth(username, password)
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("error: ", err)
	}
	bodyText, err := ioutil.ReadAll(resp.Body)
	s := string(bodyText)
	return s
}
