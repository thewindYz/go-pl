package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
)

type SIMAP map[string]interface{}

// 构造聚合基本结构
type aggItem struct {
	tName string // typeName, 聚合的分类 (bucket,metric,pipeline)
	fName string // functionName, metric聚合的函数
	args  SIMAP  // 函数的参数列表
}

// 构造聚合链表节点
type aggsNode struct {
	items []aggItem // 对应一个聚合层可以包含多个同级别的聚合
	next  *aggsNode // next
}

// // 聚合单链表
// type aggsSingleList struct {
// 	head, tail *aggsNode
// }

// // 聚合单链表初始化
// func (list *aggsSingleList) init() {
// 	list.head = nil
// 	list.tail = nil
// }

// // 聚合单链表，添加节点
// func (list *aggsSingleList) append(node *aggsNode) bool {
// 	if node == nil {
// 		return false
// 	}
// 	if list.head == nil {
// 		list.head = node
// 		list.tail = node
// 		return true
// 	}
// 	tail := list.tail
// 	tail.next = node
// 	list.tail = node
// 	return true
// }

func main() {
	al := createAggsList()
	s := traverseList(al)
	str, _ := json.Marshal(s)
	fmt.Println(str)
}

func traverseList(al []aggsNode) string {
	l := len(al)
	var p = new(SIMAP)
	for i := 0; i < l; i++ {
		ri := l - 1 - i
		parseNode(ri, al[ri], p)
	}
	s, _ := json.Marshal(*p)
	fmt.Println(string(s))
	return string(s)

	// var ptr *SIMAP
	// if nptr.next == nil {
	// 	return parseNode(nptr)
	// }

	// return parseNode(nptr) + traverseList(nptr.next)
}

func parseNode(level int, node aggsNode, ptr *SIMAP) {
	aggs := map[string]SIMAP{}
	strLevel := strconv.Itoa(level)

	for index, v := range node.items {
		strIndex := strconv.Itoa(index)
		aggsName := strings.Join(
			[]string{v.tName, v.fName, strLevel, strIndex},
			"-",
		)
		switch v.tName {
		case "metric":
			inner := SIMAP{v.fName: v.args}
			aggs[aggsName] = inner
		case "bucket":
			inner := SIMAP{v.fName: v.args}
			aggs[aggsName] = inner
		}
	}

	if *ptr != nil {
		for _, v := range aggs {
			v["aggs"] = (*ptr)["aggs"]
			break
		}
	}

	*ptr = SIMAP{
		"aggs": aggs,
	}

	return
}

func createAggsList() []aggsNode {
	dateAggs := SIMAP{"interval": "1h", "field": "createTime"}
	a1 := aggItem{
		tName: "bucket",
		fName: "date_histogram",
		args:  dateAggs,
	}

	f1Aggs := SIMAP{"field": "age", "size": 10}
	a2 := aggItem{
		tName: "bucket",
		fName: "terms",
		args:  f1Aggs,
	}

	f2Aggs := SIMAP{"field": "height", "size": 10}
	a3 := aggItem{
		tName: "bucket",
		fName: "terms",
		args:  f2Aggs,
	}

	topAggs := SIMAP{"size": 10}

	a4 := aggItem{
		tName: "metric",
		fName: "top_hits",
		args:  topAggs,
	}

	avgAggs := SIMAP{"field": "height"}
	a5 := aggItem{
		tName: "metric",
		fName: "avg",
		args:  avgAggs,
	}
	inner1 := []aggItem{
		a1,
	}
	n1 := aggsNode{
		items: inner1,
	}

	inner2 := []aggItem{
		a2,
	}
	n2 := aggsNode{
		items: inner2,
	}

	inner3 := []aggItem{
		a3,
	}
	n3 := aggsNode{
		items: inner3,
	}

	inner4 := []aggItem{
		a4, a5,
	}
	n4 := aggsNode{
		items: inner4,
	}

	al := []aggsNode{n1, n2, n3, n4}

	// al := new(aggsSingleList)
	// al.init()
	// al.append(&n1)
	// al.append(&n2)
	// al.append(&n3)
	// al.append(&n4)
	return al
}

// package main

// import (
// 	"fmt"
// 	"strconv"
// )

// func getStrInterval(i int64) string {
// 	r := (i / 1e6)
// 	return strconv.FormatInt(r, 10) + "ms"
// }

// func main() {
// 	value := int64(120000000000)
// 	// d2 := time.Duration(value) * time.Millisecond
// 	// fmt.Println(d2)
// 	// r := int64(time.Millisecond / time.Nanosecond)
// 	// fmt.Println(r)
// 	// fmt.Println(value / r)
// 	s := getStrInterval(value)
// 	fmt.Println(s)
// 	// fmt.Println(int64(time.Nanosecond))
// }
