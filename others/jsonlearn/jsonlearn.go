package main

import (
	"encoding/json"
	"fmt"
)

// Account 定义新的数据类型
type Account struct {
	Email    string
	password string
	Money    float64
}

//Account1 定义新的结构
type Account1 struct {
	Email    string  `json:"email"`
	Password string  `json:"pass_word"`
	Money    float64 `json:"money"`
}

func main() {
	l := "age"
	var ld interface{}
	err := json.Unmarshal([]byte(l), &ld)
	fmt.Println(err)
	
	// var m = map[string]string{
	// 	"f1": "field1",
	// }

	// b, err := json.Marshal(m)
	// if err != nil {
	// 	fmt.Println("err ", err)
	// }
	// s := string(b)
	// fmt.Println(string(s))
	// var res interface{}
	// by := []byte(s)
	// fmt.Println("----", by)
	// unmarshalerr := json.Unmarshal([]byte(s), &res)
	// if unmarshalerr != nil {
	// 	fmt.Println("err ", unmarshalerr)
	// }
	// fmt.Println(res)
	// fmt.Println()
	// var s1, s2 string
	// s1 = "or"
	// s2 = "and"
	// fmt.Println(s1 == "or")
	// fmt.Println(s2 == "or")
}

/*

func main() {
	account := Account{
		Email:    "测试@gmail.com",
		password: "123456",
		Money:    100.1,
	}
	rs, err := json.Marshal(account)
	if err != nil {
		fmt.Println("json marshal error: ", err)
	}
	fmt.Printf("rs的数据类型是: %T\n", rs)
	fmt.Println(rs)
	fmt.Println(string(rs))

	account1 := Account1{
		Email:    "测试@gmail.com",
		Password: "123456",
		Money:    100.1,
	}
	rs1, err := json.Marshal(account1)
	if err != nil {
		fmt.Println("json marshal error: ", err)
	}
	fmt.Printf("rs1的数据类型是: %T\n", rs1)
	fmt.Println(rs1)
	fmt.Println(string(rs1))
}

*/
