package main

import (
	"encoding/json"
	"fmt"
	"strings"
	"testing"
)

func TestStringsJoin(t *testing.T) {
	fName := "error.keyword"
	nFName := fName
	s := strings.Split(fName, ".")
	l := len(s)
	if l > 1 {
		if s[l-1] == "keyword" {
			nFName = strings.Join(s[0:l-1], ".")
		}
	}
	fmt.Println("----nfname--", nFName)
}

type ms struct {
	TookInMillis *string `json:"took,omitempty"` // search time in milliseconds
	//TookInMillis string   `json:"took,omitempty"` // search time in milliseconds
	Responses []string `json:"responses,omitempty"`
}

func TestNil(t *testing.T) {
	msPtr := new(ms)
	s := []byte(`{"took":null, "responses":["11", "22"]}`)
	err := json.Unmarshal(s, &msPtr)
	if err != nil {
		t.Log(err)
	}
	t.Log(msPtr)
}

func TestOmit(t *testing.T) {

	str := "{}"
	msPtr := new(ms)
	if err := json.Unmarshal([]byte(str), msPtr); err != nil {
		fmt.Println("error")
	}
	fmt.Println(msPtr.TookInMillis, msPtr.Responses)
}

func TestJson(t *testing.T) {
	k := `{"query":{"term":{"age":{"value":"19"}}}}`
	v := "object"
	m := map[string]string{k: v}
	res, err := json.Marshal(m)
	if err != nil {
		t.Errorf("json error!")
	}
	fmt.Println(string(res))
	var s interface{}
	err = json.Unmarshal(res, &s)
	if err != nil {
		t.Errorf("unmarshal error!")
	}
	fmt.Println(s)

	// []byte(s)
}

// 测试omitempty
type Student struct {
	Name  string `json:"name"`
	Age   int    `json:"age"`
	Grade string `json:"grade,omitempty"`
}

func TestJsonOmit(t *testing.T) {
	stu1 := Student{
		Name:  "python",
		Age:   18,
		Grade: "middle school",
	}

	stu2 := Student{
		Name: "java",
		Age:  19,
	}

	s1, _ := json.Marshal(&stu1)
	s2, _ := json.Marshal(&stu2)

	fmt.Println("stu1:", string(s1))
	fmt.Println("stu2:", string(s2))
}

func TestLength(t *testing.T) {
	s := []string{}
	lenS := len(s)
	fmt.Println(lenS)
}
