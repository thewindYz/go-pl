package main

type SIMAP map[string]interface{}

func main() {
	// var s = new(SIMAP)
	// for k, v := range s {
	// 	fmt.Println("1111", k, v)
	// }
	// fmt.Println("--end--")
	// if s == nil {
	// 	fmt.Println("----")
	// }
	// fmt.Println(len(s))
	// s["aggs"] = SIMAP{"f1": "111"}
	// str := s["aggs"]
	// res, ok := str.(SIMAP)
	// fmt.Println(res, ok)

	// var s = map[string]interface{}{}
	// fmt.Println(s == nil)
	// fmt.Println(len(s))
	// var s = new(map[string]interface{})
	// fmt.Println(*s == nil)
	// *s = map[string]interface{}{"1": "11"}
	// fmt.Println(s == nil)
	// var sptr = new(map[string]interface{})
	// ss, _ := json.Marshal(s)
	// sp, _ := json.Marshal(*sptr)
	// fmt.Printf("%s\n", ss)
	// fmt.Printf("%s\n", sp)

}

/*
func main() {
	// employeeSalary := map[string]int{
	// 	"steve": 12000,
	// 	"tom":   15000,
	// }
	// employeeSalary["jack"] = 10000
	// fmt.Println(employeeSalary)
	// var s1, s2 string
	// s1 = "or"
	// s2 = "and"
	// fmt.Println(s1 == "or")
	// fmt.Println(s2 == "or")
	var arithmetic_map = map[string]string{
		">": "gt",
		"<": "lt",
	}

	var logic_map = map[string]string{
		"and": "must",
		"or":  "should",
	}

	var term_map = map[string]string{
		"=": "term",
	}

	op := "="
	l := "age"
	r := 10

	tv, ok := term_map[op]

	if ok == true {
		iinter := map[string]interface{}{
			"value": r,
		}
		inter := map[string]interface{}{
			l: iinter,
		}
		outer := map[string]interface{}{
			tv: inter,
		}
		fmt.Println(outer)
		s, _ := json.Marshal(outer)
		fmt.Printf("%s\n", s)
	}

	lv, err := logic_map[op]
	if err == true {
		iinter := []interface{}{
			"left", "right",
		}
		inter := map[string][]interface{}{
			lv: iinter,
		}
		outer := map[string]interface{}{
			"bool": inter,
		}
		fmt.Println("outer--", outer)
		s, _ := json.Marshal(outer)
		fmt.Printf("%s\n", s)
		return
	}

	v, err := arithmetic_map[op]
	if err == true {
		fmt.Println("error", err)
		iinter := map[string]interface{}{
			v: r,
		}
		inter := map[string]interface{}{
			l: iinter,
		}
		outer := map[string]interface{}{
			"range": inter,
		}
		fmt.Println(outer)
		s, _ := json.Marshal(outer)
		fmt.Printf("%s\n", s)
	}
	return

}

// if op == ">" {

// 	iinter := map[string]int{
// 		"gt": r,
// 	}
// 	inter := map[string]interface{}{
// 		l: iinter,
// 	}
// 	res := map[string]interface{}{
// 		"range": inter,
// 	}
// 	fmt.Println(res)
// 	s, _ := json.Marshal(res)
// 	fmt.Printf("%s\n", s)
// 	fmt.Println([]byte(s))
// }
// if op == ">"{
// 	var res map[string]interface{}{
// 		"range": map[string]interface{}
// 	}

// }
// }

/*
func main() {
	employeeSalary := make(map[string]int)
	employeeSalary["steve"] = 12000
	employeeSalary["jamie"] = 15000
	employeeSalary["mike"] = 9000
	fmt.Println(employeeSalary)
}
*/
