package main

import (
	"fmt"
	"reflect"
)

type order struct {
	ordID      int
	customerID int
}

type employee struct {
	name    string
	id      int
	address string
	salary  int
	country string
}

func createQuery(q interface{}) {

	if reflect.ValueOf(q).Kind() == reflect.Struct {
		t := reflect.TypeOf(q).Name()
		query := fmt.Sprintf("insert into %s values (", t)
		v := reflect.ValueOf(q)
		rt := reflect.TypeOf(q)

		for i := 0; i < v.NumField(); i++ {
			fmt.Printf("%v\n", rt.Field(i).Name)
			switch v.Field(i).Kind() {
			case reflect.Int:
				if i == 0 {
					query = fmt.Sprintf("%s%d", query, v.Field(i).Int())
				} else {
					query = fmt.Sprintf("%s, %d", query, v.Field(i).Int())
				}
			case reflect.String:
				if i == 0 {
					query = fmt.Sprintf("%s\"%s\"", query, v.Field(i).String())
				} else {
					query = fmt.Sprintf("%s, \"%s\"", query, v.Field(i).String())
				}
			default:
				fmt.Println("unsupported type")
				return
			}

		}
		query = fmt.Sprintf("%s)", query)
		fmt.Println(query)
		return
	}
	fmt.Println("unsupported type")
}

func main() {
	o := order{
		ordID:      456,
		customerID: 56,
	}
	createQuery(o)

	e := employee{
		name:    "Naveen",
		id:      565,
		address: "Coimbatore",
		salary:  90000,
		country: "India",
	}
	createQuery(e)
	i := 90
	createQuery(i)
}

/*
func cerateQuery(q interface{}) {
	if reflect.ValueOf(q).Kind() == reflect.Struct {
		v := reflect.ValueOf(q)
		fmt.Println("number of fields ", v.NumField())
		for i := 0; i < v.NumField(); i++ {
			fmt.Printf("field: %d type %T value: %v\n", i, v.Field(i), v.Field(i))
		}
	}
}
*/

/*
func main() {
	a := 56
	x := reflect.ValueOf(a).Int()
	fmt.Printf("type: %T value: %v\n", x, x)

	b := "test"
	y := reflect.ValueOf(b).String()
	fmt.Printf("type: %T value: %v\n", y, y)
}
*/

/*
func createQuery(q interface{}) string {
	t := reflect.TypeOf(q)
	v := reflect.ValueOf(q)
	k := t.Kind()
	fmt.Println("type is ", t)
	fmt.Println("value is ", v)
	fmt.Println("kind is ", k)
	return ""
}
*/

/*
func main() {
	o := order{
		ordID:      456,
		customerID: 78,
	}
	// createQuery(o)
	cerateQuery(o)
}
*/

/*
type order struct {
	ordID      int
	customerID int
}

func createQuery(o order) string {
	i := fmt.Sprintf("insert into order values(%d, %d)", o.ordID, o.customerID)
	return i
}

func main() {
	o := order{
		ordID:      1234,
		customerID: 567,
	}
	// fmt.Println(o)
	fmt.Println(createQuery(o))
}
*/

/*
func main() {
	i := 10
	fmt.Printf("%d %T", i, i)
}
*/
