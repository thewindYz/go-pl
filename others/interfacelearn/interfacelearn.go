package main

type VowelsFinder interface {
	FindVowels() []rune
}

type MyString string
