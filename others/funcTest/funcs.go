package parser

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"

	"gitlab.jiagouyun.com/cloudcare-tools/kodo/utils"
)

//================== const ===================
//"object", "O", "logging", "L", "event", "E", "tracing", "T", "rum", "R"

const (
	// object 相关

	// OABBRNAME 缩写
	OABBRNAME = "O"
	// OFULLNAME 全称
	OFULLNAME = "object"
	// OCLASS 对象类型 分类字段
	OCLASS = "__class"
	// OTIME 对象类型 时间字段
	OTIME = "__meta.__esLastUpdateTime"

	// logging 相关

	// LABBRNAME 缩写
	LABBRNAME = "L"
	// LFULLNAME 全称
	LFULLNAME = "logging"
	// LCLASS 对象类型 分类字段
	LCLASS = "__source"
	// LCLASS = "__tags.__source.keyword"

	// LTIME 对象类型 时间字段
	LTIME = "__timestampMs"

	// keyevent 相关

	// EABBRNAME 缩写
	EABBRNAME = "E"
	// EFULLNAME 全称
	EFULLNAME = "event"
	// ECLASS 对象类型 分类字段
	ECLASS = "__source"
	// ECLASS = "__tags.__source.keyword"

	// ETIME 对象类型 时间字段
	ETIME = "__timestampMs"

	// tracing 相关

	// TABBRNAME 缩写
	TABBRNAME = "T"
	// TFULLNAME 全称
	TFULLNAME = "tracing"
	// TCLASS 对象类型 分类字段
	TCLASS = "__serviceName"
	// TCLASS = "__tags.__serviceName"

	// TTIME 对象类型 时间字段
	TTIME = "__timestampMs"

	// rum 相关

	// RABBRNAME 缩写
	RABBRNAME = "R"
	// RFULLNAME 全称
	RFULLNAME = "rum"
	// RCLASS 对象类型 分类字段
	RCLASS = "type"
	// RCLASS = "__tags.type"

	// RTIME 对象类型 时间字段
	RTIME = "__timestampMs"

	// TIMEDEFAULT 默认time字段名称
	TIMEDEFAULT = "time" // 默认的时间字段别名
)

// ESNamespace 包含 timeField, classField信息
type ESNamespace struct {
	abbrName   string
	fullName   string
	classField string
	timeField  string
}

// DFState 状态描述
type DFState struct {
	timeRangeQuery bool // 是否包含时间范围查询
	aggs           bool // 是否有聚合
	dateHg         bool // 是否有日期聚合
	bucket         bool // 是否有桶聚合
	metric         bool // 是否有指标聚合
	hasTopFuncs    bool // 非聚合，是否有top，bottom，first，last的查询
	hasDistinct    bool // 是否有distinct函数
}

// EST ESTransInfo（对应ast）
type EST struct {
	dfState     *DFState     // DFState 状态描述
	esNamespace *ESNamespace // timeField, classField
	limitSize   int          // size

	// 函数
	groupFields   []string          // 桶聚合字段列表
	groupOrders   map[string]string // 桶聚合的顺序
	distinctField string            // distinct字段

	// 请求结果解析
	AliasSlice []map[string]string // AliasSlice 别名信息
	ClassNames string              // ClassNames 指标集名称
	SortFields []string            // SortFields 选择字段有序列表
}

//==================全局变量，只读===================
var (
	objectNamespace = ESNamespace{
		abbrName:   OABBRNAME,
		fullName:   OFULLNAME,
		classField: OCLASS,
		timeField:  OTIME,
	}

	loggingNamespace = ESNamespace{
		abbrName:   LABBRNAME,
		fullName:   LFULLNAME,
		classField: LCLASS,
		timeField:  LTIME,
	}

	eventNamespace = ESNamespace{
		abbrName:   EABBRNAME,
		fullName:   EFULLNAME,
		classField: ECLASS,
		timeField:  ETIME,
	}

	tracingNamespace = ESNamespace{
		abbrName:   TABBRNAME,
		fullName:   TFULLNAME,
		classField: TCLASS,
		timeField:  TTIME,
	}

	rumNamespace = ESNamespace{
		abbrName:   RABBRNAME,
		fullName:   RFULLNAME,
		classField: RCLASS,
		timeField:  RTIME,
	}

	ESNamespaces = []*ESNamespace{
		&objectNamespace,
		&loggingNamespace,
		&eventNamespace,
		&tracingNamespace,
		&rumNamespace,
	}

	// 支持聚合函数列表
	TopFName           = "top"
	BottomFName        = "bottom"
	FirstFName         = "first"
	LastFName          = "last"
	AvgFName           = "avg"
	CountdistinctFName = "countdistinct"
	DistinctFName      = "distinct"
	TermsFName         = "terms"
	MaxFName           = "max"
	MinFName           = "min"
	PercentileFName    = "percentile"
	SumFName           = "sum"
	CountFName         = "count"

	// text 字段匹配规则
	ObjectTextFields  = []string{"__content"}
	LoggingTextFields = []string{"__content"}
	TracingTextFields = []string{"__content"}
	EventTextFields   = []string{"__content", "__title"}
	RumRegExp         = ".*(message|stack)$"

	// limit信息
	DefaultLimit       = 1000 // 查询默认的返回数量
	MaxLimit           = 5000 // 查询的最大返回数量
	DefaultGroupLimit  = 10   // 聚合默认的返回数量
	MaxGroupLimit      = 100  // 聚合最大的返回数量
	DefaultTophitLimit = 1    // tophit默认的返回数量
	ZeroLimit          = 0    // 有聚合时，查询默认返回数量

	//
	AggsIdentifier = "aggs"
	BucketAggs     = "bucket"  // 桶聚合类型名称
	QueryValue     = "value"   // query field 关键字
	AggsTophits    = "tophits" // tophits
	SelectAll      = "*"       // 查询返回所有字段

	// TopFuncs 可以转化为tophits的函数
	TopFuncs = []string{TopFName, BottomFName, FirstFName, LastFName}

	// AggsMetricFuncs 指标聚合函数集合
	// 函数名不区分大小写，ast统一lower
	AggsMetricFuncs = map[string]string{
		AvgFName:           AvgFName,      // 平均值
		CountdistinctFName: "cardinality", // ES中的cardinality不同于influxdb，定义为新函数countdistinct
		DistinctFName:      TermsFName,    // distinct使用terms实现
		MaxFName:           MaxFName,
		MinFName:           MinFName,
		PercentileFName:    "percentiles", // 百分位
		SumFName:           SumFName,
		CountFName:         "value_count", // count
	}

	// AggsBucketFuncs 桶聚合相关函数集合
	AggsBucketFuncs = map[string]string{
		AggsTophits: "top_hits",
		// AGGSGROUPSIZE: "size",
	}

	// QueryFuncs 查询函数集合
	QueryFuncs = map[string]string{
		"=":     "regexp",
		"match": "match",
	}

	// ArithmeticMap 算术运算符的映射
	ArithmeticMap = map[string]string{
		">":  "gt",
		"<":  "lt",
		">=": "gte",
		"<=": "lte",
	}

	// LogicMap 逻辑运算符的映射
	LogicMap = map[string]string{
		"and": "must",
		"or":  "should",
	}

	// LogicNotMap 逻辑运算符not
	LogicNotMap = map[string]string{
		"!=": "must_not",
	}

	// TermMap 精确查询映射
	TermMap = map[string]string{
		"=": "term",
	}

	// SortMap sort的映射关系
	SortMap = map[int]string{
		OrderDesc: "desc",
		OrderAsc:  "asc",
	}

	// 内置cast函数

	// IntFName int 函数, string -> int
	IntFName = "int"

	// FloatFName float 函数, string -> float
	FloatFName = "float"

	// NestAggFuncs 嵌套聚合函数
	// CastFuncs = []string{IntFName, FloatFName}
	NestAggFuncs = map[string][]string{
		AvgFName: []string{IntFName, FloatFName},
		MaxFName: []string{IntFName, FloatFName},
		MinFName: []string{IntFName, FloatFName},
		SumFName: []string{IntFName, FloatFName},
	}
)

//==================unit func===================

func voidToString(s interface{}) (string, error) {
	if res, ok := s.(string); ok {
		return res, nil
	}
	return "", fmt.Errorf("%v type is %T, can not convert to string", s, s)
}

// isTextField 判断是否为text类型
func (esPtr *EST) isTextField(fieldName string) (bool, error) {
	switch esPtr.esNamespace.fullName {
	case RFULLNAME: // rum
		match, err := regexp.MatchString(RumRegExp, fieldName)
		if err != nil {
			return false, err
		}
		return match, nil
	case OFULLNAME: // object
		return findStringSlice(fieldName, ObjectTextFields), nil
	case LFULLNAME: // logging
		return findStringSlice(fieldName, LoggingTextFields), nil
	case EFULLNAME: // event
		return findStringSlice(fieldName, EventTextFields), nil
	case TFULLNAME: // tracing
		return findStringSlice(fieldName, TracingTextFields), nil
	}
	return false, nil
}

func (esPtr *EST) findAliasField(fieldName string) string {
	if k, ok := esPtr.AliasSlice[1][fieldName]; ok {
		return k //如果存在别名，需要转化
	}
	return fieldName
}

// findStringSlice 查看string是否在slice string中
func findStringSlice(s string, items []string) bool {
	for _, item := range items {
		if item == s {
			return true
		}
	}
	return false
}

// appendSortFields sortField添加新的field
func (esPtr *EST) appendSortFields(fieldName string) error {
	if fieldName == esPtr.esNamespace.timeField {
		// 判断是否已经有time字段
		if find := findStringSlice(fieldName, esPtr.SortFields); find {
			return nil
		}
	}
	esPtr.SortFields = append(esPtr.SortFields, fieldName)
	return nil
}

// IsStringParam 函数参数，类型判断
func IsStringParam(ptr interface{}) bool {
	// StringLiteral, Identifier
	if _, ok := ptr.(*Identifier); ok {
		return true
	}
	if _, ok := ptr.(*StringLiteral); ok {
		return true
	}
	return false
}

// GetStringParam 获取string param
func GetStringParam(ptr interface{}) string {
	// StringLiteral, Identifier
	if rPtr, ok := ptr.(*Identifier); ok {
		return rPtr.Name
	}
	if rPtr, ok := ptr.(*StringLiteral); ok {
		return rPtr.Val
	}
	return ""
}

//==================customize struct===================

// SIMAP 自定义类型
type SIMAP map[string]interface{}

// ESMetric ES查询语句的抽象结构
// 实例化——>序列化后为ES的查询字符串
type ESMetric struct {
	Aggs   interface{}   `json:"aggs"`              //  聚合部分
	Fields []string      `json:"_source,omitempty"` //  返回列列表
	Query  SIMAP         `json:"query,omitempty"`   //  查询部分
	Size   int           `json:"size"`              //  返回数量
	Sort   []interface{} `json:"sort,omitempty"`    //  排序,可缺失
}

// 聚合基本单元
type aggsItem struct {
	tName string // typeName, 表示聚合类型（metric/bucket/pipeline)
	fName string // functionName如果是指标聚合，函数名称
	args  SIMAP  // 函数参数
	alias string // 聚合别名
}

// 聚合节点
type aggsNode struct {
	items []aggsItem // 对应一个聚合层包含多个子聚合，例如: topHits与avg同层
}

func (m *DFQuery) initEST() (*EST, error) {
	res := new(EST)

	res.dfState = new(DFState)
	res.esNamespace = new(ESNamespace)
	res.limitSize = ZeroLimit
	res.groupFields = []string{}
	res.distinctField = ""
	res.groupOrders = map[string]string{}

	res.AliasSlice = []map[string]string{
		map[string]string{}, // k:v = fieldName: aliasName
		map[string]string{}, // k:v = aliasName: fieldName
	}
	res.ClassNames = ""
	res.SortFields = []string{}
	return res, nil
}

// 判断是否有时间范围查询
func checkTimeRangeQuery(m *DFQuery, esPtr *EST) error {
	// 如果是object数据，查询不需要加上时间范围
	if m.TimeRange != nil && (m.Namespace != OFULLNAME) {
		esPtr.dfState.timeRangeQuery = true
		return nil
	}
	esPtr.dfState.timeRangeQuery = false
	return nil
}

// 判断是否有聚合
func checkDfstateAggs(m *DFQuery, esPtr *EST) error {
	dateHg := m.existDateHgAggs(esPtr)
	bucket := m.existBucketAggs(esPtr)
	metric := m.existMetricAggs(esPtr)
	res := dateHg || bucket || metric
	esPtr.dfState.aggs = res
	return nil
}

// 判断是否有时间聚合
func (m *DFQuery) existDateHgAggs(esPtr *EST) bool {
	if m.TimeRange != nil && m.TimeRange.Resolution != nil {
		esPtr.dfState.dateHg = true
		return true
	}
	esPtr.dfState.dateHg = false
	return false
}

// 判断是否有桶聚合
func (m *DFQuery) existBucketAggs(esPtr *EST) bool {
	if m.GroupBy != nil {
		esPtr.dfState.bucket = true
		return true
	}
	esPtr.dfState.bucket = false
	return false
}

// 判断是否有指标聚合或者topFuncs
func (m *DFQuery) existMetricAggs(esPtr *EST) bool {
	for _, i := range m.Targets {
		s := i.Col
		if fc, ok := s.(*FuncExpr); ok {
			if _, found := AggsMetricFuncs[fc.Name]; found {
				esPtr.dfState.metric = true
				if fc.Name == DistinctFName {
					esPtr.dfState.hasDistinct = true
				}
				return true
			}
			if find := findStringSlice(fc.Name, TopFuncs); find {
				esPtr.dfState.metric = true
				esPtr.dfState.hasTopFuncs = true
				return true
			}
		}
	}
	esPtr.dfState.metric = false
	return false
}

// 检查namespace
func checkNamespace(m *DFQuery) error {
	// 如果namespace 是缩写，转换为全称
	find := false
	for _, nPtr := range ESNamespaces {
		if m.Namespace == nPtr.abbrName {
			m.Namespace = nPtr.fullName
			find = true
			break
		} else {
			if m.Namespace == nPtr.fullName {
				find = true
				break
			}
		}
	}
	if find == false {
		return fmt.Errorf("invalid namespace")
	}
	return nil
}

// 检查是否包含指标集
func checkMetricList(m *DFQuery) error {
	if m.Names == nil && m.RegexNames == nil {
		return fmt.Errorf("no metrics")
	}
	return nil
}

// 基本检查
func basicCheck(m *DFQuery) error {
	var err error
	checkFuncs := []func(*DFQuery) error{
		checkNamespace,  // (1) 判断namespace是否正确
		checkMetricList, // (2) 判断是否有指标集
	}
	for _, f := range checkFuncs {
		err = f(m)
		if err != nil {
			return err
		}
	}
	return nil
}

// checkNestAggFuncs 判断内置聚合函数
func checkNestAggFuncs(fName string, fc *StaticCast) error {
	// 判断是否支持内置函数
	if _, ok := NestAggFuncs[fName]; !ok {
		return fmt.Errorf("%s func unsupport nest func", fName)
	}
	// 判断是否是合法的内置函数
	ifName := ""
	if fc.IsInt {
		ifName = IntFName
	} else {
		if fc.IsFloat {
			ifName = FloatFName
		}
	}
	if find := findStringSlice(ifName, NestAggFuncs[fName]); !find {
		return fmt.Errorf("%s func unsupport nest func %s", fName, ifName)
	}
	// // 判断内层函数参数
	// if len(fc.Param) != 1 {
	// 	return fmt.Errorf("%s func should have and at most one field name", fc.Name)
	// }
	// if IsStringParam(fc.Param[0]) == false {
	// 	return fmt.Errorf("%s func first param should be field name", fc.Name)
	// }
	return nil
}

// checkAggsFuncs 检查是否是合法的聚合函数
func checkAggsFuncs(m *DFQuery, esPtr *EST) error {

	for _, t := range m.Targets {
		if fc, ok := (t.Col).(*FuncExpr); ok {
			found := false // 针对每个函数判断
			// 1) 判断是否是指标聚合
			if _, ok := AggsMetricFuncs[fc.Name]; ok {
				found = true
				return checkAggsESFuncs(fc)
			}
			// 2) 判断是否是top等函数
			if find := findStringSlice(fc.Name, TopFuncs); find {
				return checkAggsTopFuncs(fc)
			}
			if !found {
				return fmt.Errorf("unsupport func %s", fc.Name)
			}
		}
	}
	return nil
}

// checkGroupByFields 检查groupby聚合字段
func checkGroupByFields(m *DFQuery, esPtr *EST) error {
	if esPtr.dfState.bucket {
		fields := []string{}
		for _, i := range m.GroupBy.List {
			if IsStringParam(i) == false {
				// 只支持对field桶聚合
				return fmt.Errorf("only support field with group by")
			}
			fieldName := GetStringParam(i)
			if find := findStringSlice(fieldName, fields); find {
				// 多层聚合，不能对相同的field
				return fmt.Errorf("each field can only be grouped once")
			}
			fields = append(fields, fieldName)
			esPtr.groupFields = fields
		}
	}
	return nil
}

// setAggsOrders 设置桶聚合相关的order
func checkAggsOrders(m *DFQuery, esPtr *EST) error {
	// 如果存在order by
	if m.OrderBy != nil {
		sortType := SortMap[m.OrderBy.Opt]
		for _, item := range m.OrderBy.List {
			if IsStringParam(item) {
				fieldName := GetStringParam(item)
				esPtr.groupOrders[fieldName] = sortType
			}
		}
	}
	return nil
}

// checkLimitSize 检查limit size
func checkLimitSize(m *DFQuery, esPtr *EST) error {
	// (1）有聚合，size limit最大值为100
	if esPtr.dfState.aggs {
		if m.Limit == nil {
			if esPtr.dfState.hasDistinct {
				esPtr.limitSize = DefaultLimit
			} else {
				esPtr.limitSize = DefaultGroupLimit
			}

		} else {
			inputLimit := int(m.Limit.Limit)
			if inputLimit > MaxGroupLimit {
				esPtr.limitSize = MaxGroupLimit // ast 会自动填充
				// return fmt.Errorf("aggs max limit size is %d, but you set %d", MaxGroupLimit, inputLimit)
			}
			esPtr.limitSize = inputLimit
		}

	} else { // (2) 没有聚合
		if m.Limit == nil {
			esPtr.limitSize = DefaultLimit
		} else {
			inputLimit := int(m.Limit.Limit)
			if inputLimit > MaxLimit {
				return fmt.Errorf("query max limit size is %d, but you set %d", MaxLimit, inputLimit)
			}
			esPtr.limitSize = inputLimit
		}
	}
	return nil
}

// check with EST
func checkAndSetEST(m *DFQuery, esPtr *EST) error {
	var err error
	// (1) 初始化状态后，检查
	checkFuncs := []func(*DFQuery, *EST) error{
		checkDfstateAggs,
		checkTimeRangeQuery,
		checkESNamespace,
		checkAggsFuncs,     // 检查是否是合法函数
		checkGroupByFields, // 检查桶聚合
		checkLimitSize,     // 检查并且设置limit size
		checkAggsOrders,    // 检查设置 groupOrders
	}
	for _, f := range checkFuncs {
		err = f(m, esPtr)
		if err != nil {
			return err
		}
	}
	return nil
}

func (m *DFQuery) checkValid() (*EST, error) {
	var err error
	// (1) basic check
	err = basicCheck(m)
	if err != nil {
		return nil, err
	}
	// (2) 初始化EST
	esPtr, err := m.initEST()
	if err != nil {
		return nil, err
	}
	// (3) 更新EST
	err = checkAndSetEST(m, esPtr)
	if err != nil {
		return nil, err
	}
	return esPtr, nil

}

// checkESNamespace 设置timeField, classField
func checkESNamespace(m *DFQuery, esPtr *EST) error {
	for _, nPtr := range ESNamespaces {
		if nPtr.fullName == m.Namespace {
			esPtr.esNamespace = nPtr
			break
		}
	}
	return nil
}

// getTimeStamp 获取时间聚合信息
func (m *DFQuery) getTimeStamp(esPtr *EST) (string, error) {
	var interval int64
	interval = int64(m.TimeRange.Resolution.Duration / time.Millisecond) // 单位:毫秒
	if interval < 1 {
		return "", fmt.Errorf("time interval should large than 1ms")
	}
	return strconv.FormatInt(interval, 10) + "ms", nil

}

// genDateHgNode 生成时间范围聚合
func (m *DFQuery) genDateHgNode(esPtr *EST) (*aggsNode, error) {
	var inner = SIMAP{}
	interval, err := m.getTimeStamp(esPtr)
	if err != nil {
		return nil, err
	}
	inner["field"] = esPtr.esNamespace.timeField
	inner["interval"] = interval
	var item = aggsItem{
		fName: "date_histogram",
		tName: "bucket",
		args:  inner,
		alias: utils.Times,
	}
	var items = []aggsItem{item}
	return &aggsNode{items: items}, nil
}

// checkAggsTopFuncs 检查top等函数
func checkAggsTopFuncs(fc *FuncExpr) error {
	lenParam := len(fc.Param)
	switch fc.Name {
	case TopFName, BottomFName:
		//必须含有size的值
		if lenParam != 2 {
			return fmt.Errorf("%s func must have two params, one field name, one size", fc.Name)
		}
		if IsStringParam(fc.Param[0]) == false {
			// 第一个参数是字符串
			return fmt.Errorf("%s func first param should be field name", fc.Name)
		}
		if _, ok := fc.Param[1].(*NumberLiteral); !ok {
			// 第二个参数是数值
			return fmt.Errorf("%s func second param should be size value", fc.Name)
		}
	case FirstFName, LastFName:
		//必须含有size的值
		if lenParam != 1 {
			return fmt.Errorf("%s func should have and at most one field name", fc.Name)
		}
		if IsStringParam(fc.Param[0]) == false {
			// 第一个参数是字符串
			return fmt.Errorf("%s func first param should be field name", fc.Name)
		}
	}
	return nil
}

// checkAggsESFuncs 检查es支持的原生函数
func checkAggsESFuncs(fc *FuncExpr) error {
	lenParam := len(fc.Param)
	//必须含有size的值
	if lenParam != 1 {
		return fmt.Errorf("%s func should have and at most one field name", fc.Name)
	}
	if ifc, ok := fc.Param[0].(*StaticCast); ok {
		// 检查内置聚合函数
		return checkNestAggFuncs(fc.Name, ifc)
	}
	if IsStringParam(fc.Param[0]) == false {
		return fmt.Errorf("%s func first param should be field name", fc.Name)
	}
	return nil
}

// genBucketNodes 生成桶聚合
func (m *DFQuery) genBucketNodes(esPtr *EST) (*[]aggsNode, error) {
	var res []aggsNode
	for _, fieldName := range esPtr.groupFields {
		// 获取索引时候的fieldName，
		// 如果使用aliasName，需要转为实际fileName
		var aliasName = fieldName
		fieldName = esPtr.findAliasField(fieldName)

		// 如果是text字段，桶聚合需要使用fName.keyword
		ok, err := esPtr.isTextField(fieldName)
		if err != nil {
			return nil, err
		}
		if ok {
			fieldName = fieldName + ".keyword"
		}

		i := aggsItem{
			tName: BucketAggs,
			fName: TermsFName,
			args: SIMAP{
				"field": fieldName,
				"size":  esPtr.limitSize,
			},
			alias: formatAliasField(aliasName),
		}
		// 添加桶聚合排序
		if groupOrder, ok := esPtr.groupOrders[fieldName]; ok {
			i.args["order"] = map[string]string{"_key": groupOrder}
		}
		node := aggsNode{items: []aggsItem{i}}
		res = append(res, node)
	}
	return &res, nil
}

// getCastScript
func getCastScript(fName, fieldName string) interface{} {
	source := ""
	switch fName {
	case IntFName:
		source = fmt.Sprintf(
			`if (doc['%s'].size() > 0) {Integer.parseInt(doc['%s'].value)}`,
			fieldName,
			fieldName,
		)
	case FloatFName:
		source = fmt.Sprintf(
			`if (doc['%s'].size() > 0) {Float.parseFloat(doc['%s'].value)}`,
			fieldName,
			fieldName,
		)
	}
	return SIMAP{"source": source}
}

//genNestMetricItem 生成一个包含内置函数的metric聚合
func genNestMetricItem(esPtr *EST, fName string, fc *FuncExpr, alias string) (*aggsItem, string, error) {
	ifc, _ := fc.Param[0].(*StaticCast)
	// fieldName := GetStringParam(ifc.Param[0])
	fieldName := ifc.Val.String()
	var args = SIMAP{}
	// 是否是别名
	fieldName = esPtr.findAliasField(fieldName)
	// 如果是text字段，桶聚合需要使用fName.keyword
	ok, err := esPtr.isTextField(fieldName)
	if err != nil {
		return nil, "", err
	}
	if ok {
		fieldName = fieldName + ".keyword"
	}

	ifName := ""
	if ifc.IsInt {
		ifName = IntFName
	} else {
		if ifc.IsFloat {
			ifName = FloatFName
		}
	}

	castScript := getCastScript(ifName, fieldName)
	args["script"] = castScript
	var res = aggsItem{
		fName: fName,
		tName: "metric",
		args:  args,
	}

	if alias != "" {
		res.alias = alias
	} else {
		rk := formatAliasField(fieldName)
		res.alias = fc.Name + "_" + rk // 指标聚合，列名为 输入函数名_字段名，非es中函数名
	}
	esPtr.appendSortFields(esPtr.esNamespace.timeField)
	esPtr.appendSortFields(res.alias)
	return &res, res.alias, nil
}

//genMetricItem 生成一个metric聚合
func genMetricItem(esPtr *EST, fName string, fc *FuncExpr, alias string) (*aggsItem, string, error) {

	// 如果有内层函数
	if _, ok := fc.Param[0].(*StaticCast); ok {
		return genNestMetricItem(esPtr, fName, fc, alias)
	}

	fieldName := GetStringParam(fc.Param[0])

	var args = SIMAP{}
	// 是否是别名
	fieldName = esPtr.findAliasField(fieldName)
	// 如果是text字段，桶聚合需要使用fName.keyword
	ok, err := esPtr.isTextField(fieldName)
	if err != nil {
		return nil, "", err
	}
	if ok {
		fieldName = fieldName + ".keyword"
	}
	args["field"] = fieldName
	var res = aggsItem{
		fName: fName,
		tName: "metric",
		args:  args,
	}

	if alias != "" {
		res.alias = alias
	} else {
		rk := formatAliasField(fieldName)
		res.alias = fc.Name + "_" + rk // 指标聚合，列名为 输入函数名_字段名，非es中函数名
	}
	esPtr.appendSortFields(esPtr.esNamespace.timeField)
	esPtr.appendSortFields(res.alias)
	return &res, res.alias, nil
}

//genDistinctItem 生成terms聚合
func genDistinctItem(m *DFQuery, esPtr *EST, fc *FuncExpr, alias string) (*aggsItem, string, error) {
	fieldName := GetStringParam(fc.Param[0])
	rFieldName := esPtr.findAliasField(fieldName) //如果存在别名，需要转化

	// 如果是text字段，terms聚合需要使用fName.keyword
	ok, err := esPtr.isTextField(rFieldName)
	if err != nil {
		return nil, "", err
	}
	if ok {
		rFieldName = rFieldName + ".keyword"
	}

	res := aggsItem{
		tName: BucketAggs,
		fName: TermsFName,
		args:  SIMAP{"field": rFieldName, "size": DefaultLimit},
	}
	esPtr.distinctField = rFieldName
	if alias != "" {
		res.alias = alias
	} else {
		aFieldName := formatAliasField(fieldName)
		res.alias = fc.Name + "_" + aFieldName // 指标聚合，列名为 输入函数名_字段名，非es中函数名
	}
	esPtr.appendSortFields(esPtr.esNamespace.timeField)
	esPtr.appendSortFields(res.alias)
	return &res, res.alias, nil
}

// genTopHitsArgs 获取tophits聚合内部属性
func genTopHitsArgs(esPtr *EST, fc *FuncExpr) (SIMAP, error) {
	res := SIMAP{}
	source := []string{}
	size := "1"
	sort := []interface{}{}
	outer := SIMAP{}
	fieldName := GetStringParam(fc.Param[0])
	// 真正的fieldName, column值可能是别名
	fieldName = esPtr.findAliasField(fieldName)

	// 如果是text字段，桶聚合需要使用fName.keyword
	ok, err := esPtr.isTextField(fieldName)
	if err != nil {
		return nil, err
	}
	if ok {
		fieldName = fieldName + ".keyword"
	}

	// (1) 不同类型，对应不同的size和sort

	switch fc.Name {
	case TopFName:
		size = fc.Param[1].String()
		inner := map[string]string{"order": "desc"}
		outer = SIMAP{fieldName: inner}

	case BottomFName:
		size = fc.Param[1].String()
		inner := map[string]string{"order": "asc"}
		outer = SIMAP{fieldName: inner}

	case FirstFName:

		inner := map[string]string{"order": "desc"}
		outer = SIMAP{esPtr.esNamespace.timeField: inner}

	case LastFName:

		inner := map[string]string{"order": "asc"}
		outer = SIMAP{esPtr.esNamespace.timeField: inner}
	}

	sort = append(sort, outer)
	source = append(source, esPtr.esNamespace.timeField)
	source = append(source, fieldName)
	esPtr.appendSortFields(esPtr.esNamespace.timeField) // 添加time字段
	esPtr.SortFields = append(esPtr.SortFields, fieldName)

	// (2) 字段列表
	res["_source"] = source
	res["size"] = size
	res["sort"] = sort
	return res, nil

}

// genTophits 针对top,bottom,first,last使用tophits实现
func genTopHitsItem(esPtr *EST, fc *FuncExpr, alias string) (*aggsItem, string, error) {

	args, err := genTopHitsArgs(esPtr, fc)
	if err != nil {
		return nil, "", err
	}

	var res = aggsItem{
		fName: AggsBucketFuncs[AggsTophits],
		tName: "metric",
		args:  args,
	}

	fieldName := GetStringParam(fc.Param[0])

	if alias != "" {
		res.alias = alias
		// top函数，需要将field列名替换为函数别名
		esPtr.AliasSlice[0][fieldName] = alias
		esPtr.AliasSlice[1][alias] = fieldName
	} else {
		res.alias = fc.Name + "_" + fieldName
	}

	return &res, res.alias, nil
}

// genMetricNode
func genMetricNode(esPtr *EST, t *Target, m *DFQuery) (*aggsItem, string, error) {
	var (
		talias  string
		itemPtr *aggsItem
		err     error
	)
	if fc, ok := (t.Col).(*FuncExpr); ok {

		// （1）ES原生支持的指标函数 (max, min等)
		if fName, find := AggsMetricFuncs[fc.Name]; find {

			// distinct函数使用terms实现
			if fc.Name == "distinct" {
				itemPtr, talias, err = genDistinctItem(m, esPtr, fc, t.Alias)
				if err != nil {
					return nil, "", fmt.Errorf("distinct aggs error")
				}
			} else {

				itemPtr, talias, err = genMetricItem(esPtr, fName, fc, t.Alias)
				if err != nil {
					return nil, "", fmt.Errorf("metric aggs error")
				}
			}
		}

		// (2) 通过组合查询语句，构造的函数(top, bottom, first, last)
		if find := findStringSlice(fc.Name, TopFuncs); find {
			itemPtr, talias, err = genTopHitsItem(esPtr, fc, t.Alias)
			if err != nil {
				return nil, "", fmt.Errorf("tophits aggs error")
			}
			return itemPtr, talias, nil
		}
	}
	return itemPtr, talias, nil
}

// genMetricNodes 生成指标聚合
func (m *DFQuery) genMetricNodes(esPtr *EST) (*aggsNode, error) {
	var res = new(aggsNode)
	var items = []aggsItem{}
	var mNames = []string{}
	for i, t := range m.Targets {
		itemPtr, talias, err := genMetricNode(esPtr, t, m)
		if err != nil {
			return res, err
		}
		if itemPtr != nil {
			items = append(items, *itemPtr)
		}
		// target 别名
		m.Targets[i].Talias = talias
		// 同一层的指标聚合，不能是相同字段相同函数
		for _, mN := range mNames {
			if talias == mN {
				return nil, fmt.Errorf("The same field with function name should only appear once")
			}
		}
		mNames = append(mNames, talias)
	}
	res.items = items
	return res, nil
}

// genDistinctTophitsNodes
func genDistinctTophitsNodes(m *DFQuery, esPtr *EST) (*aggsNode, error) {
	var res = new(aggsNode)

	args := SIMAP{}
	// （1）args _source
	source := []string{esPtr.esNamespace.timeField}
	source = append(source, esPtr.distinctField)
	args["_source"] = source
	// （2）args size
	args["size"] = DefaultTophitLimit
	esPtr.SortFields = append(esPtr.SortFields, esPtr.esNamespace.timeField)
	esPtr.SortFields = append(esPtr.SortFields, esPtr.distinctField)

	var topAgg = aggsItem{
		fName: AggsBucketFuncs[AggsTophits],
		tName: "metric",
		args:  args,
	}
	topAgg.alias = AggsBucketFuncs[AggsTophits]
	res.items = []aggsItem{topAgg}

	return res, nil
}

// genTophitsNodes 生成tophits聚合
func (m *DFQuery) genTophitsNodes(esPtr *EST) (*aggsNode, error) {
	var res = new(aggsNode)

	args := SIMAP{}
	// （1）args _source
	args["_source"] = esPtr.SortFields
	// （2）args size
	args["size"] = esPtr.limitSize
	// （3）args sort, 默认是时间逆序
	sort := []interface{}{}
	inner := map[string]string{"order": "desc"}
	outer := SIMAP{esPtr.esNamespace.timeField: inner}
	sort = append(sort, outer)
	args["sort"] = sort

	var topAgg = aggsItem{
		fName: AggsBucketFuncs[AggsTophits],
		tName: "metric",
		args:  args,
	}
	topAgg.alias = AggsBucketFuncs[AggsTophits]
	res.items = []aggsItem{topAgg}
	return res, nil
}

// 字段别名format
func formatAliasField(fName string) string {
	res := ""
	s := strings.Split(fName, ".")
	l := len(s)
	if l > 1 {
		if s[l-1] == "keyword" {
			res = s[l-2]
		} else {
			res = s[l-1]
		}
	} else {
		res = s[0]
	}
	return res
}

// formatKeywordField source中字段格式化
// func formatKeywordField(fName string) string {
// 	nFName := fName
// 	s := strings.Split(fName, ".")
// 	l := len(s)
// 	if l > 1 {
// 		if s[l-1] == "keyword" {
// 			nFName = strings.Join(s[0:l-1], ".")
// 		}
// 	}
// 	return nFName
// }

// ESQL 用于ElasticSearch实例化
// 返回值为elasticsearch的查询语句
func (m *DFQuery) ESQL() (interface{}, error) {

	// var res = map[string]string{}
	var esPtr *EST

	// step1: 基本合法性判断
	esPtr, err := m.checkValid()
	if err != nil {
		return "", err
	}

	// step2: 通过metric信息，实例化ESMetric
	var em ESMetric
	em, err = transport(m, esPtr)

	if err != nil {
		return "", fmt.Errorf("metric transport error, %s", err)
	}

	qRes, err := json.Marshal(em)

	if err != nil {
		return "", fmt.Errorf("json marshal error, %s", err)
	}

	// res["dql"] = string(qRes)
	esAlias := map[string]string{}
	for k, v := range esPtr.AliasSlice[0] {
		rk := formatAliasField(k)
		esAlias[rk] = v
	}

	// 将解析信息添加到AST上，用于结果解析
	estResPtr := &ESTRes{
		Alias:      esAlias,
		ClassNames: esPtr.ClassNames,
		SortFields: esPtr.SortFields,
		Show:       false,
	}
	helper := &Helper{
		ESTResPtr: estResPtr,
	}
	m.Helper = helper
	return string(qRes), nil
}

// transport 转换函数
func transport(m *DFQuery, esPtr *EST) (ESMetric, error) {
	var em ESMetric
	// 1）fields
	fp, err := fieldsTransport(m, esPtr)
	if err != nil {
		return em, err
	}
	// 2) 聚合
	ap, err := aggsTransport(m, esPtr)
	if err != nil {
		return em, err
	}
	// 3）查询
	qp, err := queryTransport(m, esPtr)
	if err != nil {
		return em, err
	}
	// 4) size
	sp, err := sizeTransport(m, esPtr)
	if err != nil {
		return em, err
	}
	// 5) sort
	stp, err := sortTransport(m, esPtr)
	if err != nil {
		return em, err
	}
	em.Aggs = ap
	em.Query = qp
	em.Fields = fp
	em.Size = sp
	em.Sort = stp

	return em, nil
}

// queryTransport query(查询)部分的转换
func queryTransport(m *DFQuery, esPtr *EST) (SIMAP, error) {
	var queryPart = SIMAP{}
	var arrayRes []interface{}

	// 添加 class过滤器
	classRes, err := classTermQuery(m, esPtr)
	if err != nil {
		return queryPart, err
	}
	arrayRes = append(arrayRes, classRes)

	// 添加时间范围过滤器
	if esPtr.dfState.timeRangeQuery {
		timeRes, err := timeRangeQuery(m, esPtr)
		if err != nil {
			return queryPart, err
		}
		for _, v := range timeRes {
			arrayRes = append(arrayRes, v)
		}
	}

	// 添加 filter过滤器
	filters, err := filterQuery(m, esPtr)
	if err != nil {
		return queryPart, err
	}
	for _, i := range filters {
		arrayRes = append(arrayRes, i)
	}

	// 添加 exist 过滤器(针对top, bottom, first, last 函数)
	if esPtr.dfState.hasTopFuncs {
		filters, err := existQuery(m, esPtr)
		if err != nil {
			return queryPart, err
		}
		for _, i := range filters {
			arrayRes = append(arrayRes, i)
		}
	}

	var mustQuery = SIMAP{
		"must": arrayRes,
	}
	var boolQuery = SIMAP{
		"bool": mustQuery,
	}

	return boolQuery, nil
}

// filterQuery filter过滤器
func filterQuery(m *DFQuery, esPtr *EST) ([]interface{}, error) {
	var arrayRes []interface{}
	for _, node := range m.WhereCondition {
		if pNode, ok := node.(*ParenExpr); ok {
			s, err := pNode.esQL(esPtr)
			if err != nil {
				return arrayRes, err
			}
			arrayRes = append(arrayRes, s)
		} else {
			if bNode, ok := node.(*BinaryExpr); ok {
				s, err := bNode.esQL(esPtr)
				if err != nil {
					return arrayRes, err
				}
				arrayRes = append(arrayRes, s)
			}
		}
	}
	return arrayRes, nil
}

// existQuery exist过滤器
func existQuery(m *DFQuery, esPtr *EST) ([]interface{}, error) {
	var arrayRes []interface{}
	for _, t := range m.Targets {
		if fc, ok := (t.Col).(*FuncExpr); ok {
			switch fc.Name {
			case TopFName, BottomFName, FirstFName, LastFName:
				fieldName := GetStringParam(fc.Param[0])
				inner := map[string]string{
					"field": fieldName,
				}
				outer := map[string]interface{}{
					"exists": inner,
				}
				arrayRes = append(arrayRes, outer)
			}
		}
	}
	return arrayRes, nil
}

// aggsTransport aggs(聚合)部分的转换
func aggsTransport(m *DFQuery, esPtr *EST) (interface{}, error) {
	var res = SIMAP{}
	if esPtr.dfState.aggs == false {
		return res, nil
	}
	// 生成聚合节点列表
	al, err := genAggsNodeList(m, esPtr)
	if err != nil {
		return res, err
	}
	// 解析聚合节点列表
	res, err = parseAggsNodeList(al)
	if err != nil {
		return res, err
	}
	return res[AggsIdentifier], nil
}

// genAggsNodeList 生成聚合列表
func genAggsNodeList(m *DFQuery, esPtr *EST) ([]aggsNode, error) {
	var nodeList = []aggsNode{}
	// 1) 生成时间范围聚合
	if esPtr.dfState.dateHg {
		nodePtr, err := m.genDateHgNode(esPtr)
		if err != nil {
			return nodeList, err
		}
		nodeList = append(nodeList, *nodePtr)
	}
	// 2）生成桶聚合
	if esPtr.dfState.bucket {
		nodePtr, err := m.genBucketNodes(esPtr)
		if err != nil {
			return nodeList, err
		}
		for _, v := range *nodePtr {
			nodeList = append(nodeList, v)
		}
	}
	// 3）生成指标聚合
	if esPtr.dfState.metric {
		nodePtr, err := m.genMetricNodes(esPtr)
		if err != nil {
			return nodeList, err
		}
		if len(nodePtr.items) > 0 {
			nodeList = append(nodeList, *nodePtr)
		}
		// 如果有时间聚合，time字段名称改变
		// if esPtr.dfState.dateHg {
		// 	for i, v := range esPtr.SortFields {
		// 		if v == esPtr.esNamespace.timeField {
		// 			esPtr.SortFields[i] = TIMEDEFAULT
		// 		}
		// 	}
		// }
	}
	// 4) 如果有bucket聚合但是没有指标聚合，需要添加tophits
	if esPtr.dfState.bucket && esPtr.dfState.metric == false {
		nodePtr, err := m.genTophitsNodes(esPtr)
		if err != nil {
			return nodeList, err
		}
		nodeList = append(nodeList, *nodePtr)
	}
	return nodeList, nil
}

func parseAggsNodeList(al []aggsNode) (SIMAP, error) {
	var p = new(SIMAP)
	l := len(al)
	for i := 0; i < l; i++ {
		ri := l - 1 - i
		err := al[ri].parse(ri, p)
		if err != nil {
			return nil, err
		}
	}
	return *p, nil
}

// parse 解析聚合层
func (node *aggsNode) parse(level int, ptr *SIMAP) error {
	aggs := map[string]SIMAP{}
	strLevel := strconv.Itoa(level)

	for index, v := range node.items {
		aggsName := ""
		strIndex := strconv.Itoa(index)
		if v.alias != "" {
			aggsName = v.alias //请求中带有聚合别名
		} else {
			aggsName = strings.Join( //自动生成
				[]string{v.tName, v.fName, strLevel, strIndex},
				"-",
			)
		}
		inner := SIMAP{v.fName: v.args}
		aggs[aggsName] = inner
	}

	if *ptr != nil {
		for _, v := range aggs {
			v[AggsIdentifier] = (*ptr)[AggsIdentifier]
			break
		}
	}

	*ptr = SIMAP{
		AggsIdentifier: aggs,
	}

	return nil
}

// ESQL groupby
func (x *GroupBy) ESQL() (interface{}, error) {
	var res = []string{}
	if x == nil {
		return res, nil
	}
	for _, i := range x.List {
		str := i.String() // Identifier.ESQL()
		// 多层聚合，不能对相同的field
		for _, item := range res {
			if item == str {
				return res, fmt.Errorf("each field can only be grouped once")
			}
		}
		res = append(res, str)
	}
	return res, nil
}

// 解析targets
func getQueryFields(m *DFQuery, esPtr *EST) ([]string, bool, error) {
	var fieldsPart = []string{}
	var sAll = false
	for _, target := range m.Targets {
		// 查询时候，target默认没有函数，都是字符串
		if IsStringParam(target.Col) {
			fieldName := GetStringParam(target.Col)
			// 查询*
			if fieldName == SelectAll {
				sAll = true
			}
			// 有别名
			if target.Alias != "" {
				if _, ok := esPtr.AliasSlice[1][target.Alias]; ok {
					return fieldsPart, sAll, fmt.Errorf("can not use same alias name %s", target.Alias)
				}
				esPtr.AliasSlice[0][fieldName] = target.Alias
				esPtr.AliasSlice[1][target.Alias] = fieldName
			}
			fieldsPart = append(fieldsPart, fieldName)
		}
	}
	return fieldsPart, sAll, nil
}

// fields 部分的转换, 即ES DSL中的_source字段
func fieldsTransport(m *DFQuery, esPtr *EST) ([]string, error) {

	//  默认都有time列，别名为time
	esPtr.AliasSlice[0][esPtr.esNamespace.timeField] = TIMEDEFAULT
	esPtr.AliasSlice[1][TIMEDEFAULT] = esPtr.esNamespace.timeField

	fieldsPart, selectAll, err := getQueryFields(m, esPtr)
	if err != nil {
		return fieldsPart, err
	}
	// _source为[]或者 ["*"], 表示返回所有字段
	if len(fieldsPart) == 0 || selectAll == true {
		return []string{}, nil
	}
	// 添加time字段, 默认添加在第一列
	if find := findStringSlice(esPtr.esNamespace.timeField, fieldsPart); !find {
		newFieldsPart := []string{esPtr.esNamespace.timeField}
		for _, fp := range fieldsPart {
			newFieldsPart = append(newFieldsPart, fp)
		}
		fieldsPart = newFieldsPart
	}
	esPtr.SortFields = fieldsPart
	return fieldsPart, nil
}

//获取size
func sizeTransport(m *DFQuery, esPtr *EST) (int, error) {
	if esPtr.dfState.aggs {
		return ZeroLimit, nil
	}
	return esPtr.limitSize, nil
}

//获取sort
func sortTransport(m *DFQuery, esPtr *EST) ([]interface{}, error) {
	var res = []interface{}{}
	if esPtr.dfState.aggs == true {
		return res, nil
	}

	// 默认加上时间逆序排序
	inner := map[string]string{"order": "desc"}
	outer := SIMAP{esPtr.esNamespace.timeField: inner}
	res = append(res, outer)

	if m.OrderBy != nil {
		sortType := SortMap[m.OrderBy.Opt]
		for _, item := range m.OrderBy.List {
			if IsStringParam(item) {
				fieldName := GetStringParam(item)
				inner := map[string]string{"order": sortType}
				outer := SIMAP{fieldName: inner}
				res = append(res, outer)
			}
		}
		return res, nil
	}
	return res, nil

}

// classTermQuery 类型的过滤器
func classTermQuery(m *DFQuery, esPtr *EST) (SIMAP, error) {
	var shoulds = []SIMAP{}
	namesSlice := []string{}
	className := esPtr.esNamespace.classField
	if m.Names != nil {
		for _, v := range m.Names {
			s, err := esPtr.basicTermQuery("term", className, v, QueryValue)
			if err != nil {
				return nil, err
			}
			item, _ := s.(SIMAP)
			namesSlice = append(namesSlice, v)
			shoulds = append(shoulds, item)
		}
	}
	if m.RegexNames != nil {
		for _, v := range m.RegexNames {
			r, err := v.ESQL()
			if err != nil {
				return nil, err
			}
			str, err := voidToString(r)
			if err != nil {
				return nil, err
			}
			s, err := esPtr.basicTermQuery("regexp", className, str, QueryValue)
			if err != nil {
				return nil, err
			}
			if item, ok := s.(SIMAP); ok {
				namesSlice = append(namesSlice, str)
				shoulds = append(shoulds, item)
			} else {
				return nil, fmt.Errorf("can not get class query")
			}
		}
	}
	// ClassNames, 分类名称
	esPtr.ClassNames = strings.Join(namesSlice, ", ")

	// bool should
	var inner = SIMAP{"should": shoulds}
	return SIMAP{"bool": inner}, nil

}

// timeRangeQuery 时间范围的过滤器
func timeRangeQuery(m *DFQuery, esPtr *EST) ([]interface{}, error) {
	var start, end interface{}
	if m.TimeRange.Start == nil {
		start = time.Now().UnixNano() / int64(time.Millisecond)
	} else {
		start, _ = m.TimeRange.Start.ESQL() // 单位:毫秒
	}
	if m.TimeRange.End == nil {
		end = time.Now().UnixNano() / int64(time.Millisecond)
	} else {
		end, _ = m.TimeRange.End.ESQL() // 单位:毫秒
	}
	s, _ := rangeTermQuery("gte", esPtr.esNamespace.timeField, strconv.FormatInt(start.(int64), 10))
	e, _ := rangeTermQuery("lte", esPtr.esNamespace.timeField, strconv.FormatInt(end.(int64), 10))
	return []interface{}{s, e}, nil
}

// boolCompoundQuery, 复合bool查询
func boolCompoundQuery(v string, l interface{}, r interface{}) (interface{}, error) {
	iinter := []interface{}{
		l, r,
	}
	inter := map[string][]interface{}{
		v: iinter,
	}
	outer := SIMAP{
		"bool": inter,
	}
	return outer, nil
}

// boolNotQuery, must_not查询
func (esPtr *EST) boolNotQuery(v string, l interface{}, r interface{}) (interface{}, error) {
	s, err := esPtr.basicTermQuery("term", l, r, QueryValue)
	if err != nil {
		return nil, err
	}
	iinter := []interface{}{s}
	inter := map[string][]interface{}{
		v: iinter,
	}
	outer := SIMAP{
		"bool": inter,
	}
	return outer, nil
}

// 基本的term精确值匹配
func (esPtr *EST) basicTermQuery(fName string, lName, rValue interface{}, rName string) (interface{}, error) {
	strLName, err := voidToString(lName)
	if err != nil {
		return nil, err
	}
	strRValue, err := voidToString(rValue)
	if err != nil {
		return nil, err
	}
	// 非match查询, 如果是text字段，需要使用keyword
	if rName != "query" {
		ok, err := esPtr.isTextField(strLName)
		if err != nil {
			return nil, err
		}
		if ok {
			strLName = strLName + ".keyword"
		}
	}
	iinter := SIMAP{
		rName: strRValue,
	}
	inter := SIMAP{
		strLName: iinter,
	}
	outer := SIMAP{
		fName: inter,
	}
	return outer, nil
}

// rangeTermQuery 范围查询
// 示例:
// {
// 	"range": {
// 		"age": {
// 			"lt": "30"
// 		}
// 	}
// }
func rangeTermQuery(v string, l interface{}, r interface{}) (interface{}, error) {
	strL, err := voidToString(l)
	if err != nil {
		return nil, err
	}
	strR, err := voidToString(r)
	if err != nil {
		return nil, err
	}

	iinter := SIMAP{
		v: strR,
	}
	inter := SIMAP{
		strL: iinter,
	}
	outer := SIMAP{
		"range": inter,
	}
	return outer, nil
}

// ESQL BinaryExpr
func (x *BinaryExpr) ESQL() (interface{}, error) {
	return nil, nil
}

// ESQL 对BinaryExpr结构的解析
func (x *BinaryExpr) esQL(esPtr *EST) (interface{}, error) {
	var (
		isRegex bool
		v       string
		ok      bool
		err     error
		l, r    interface{}
	)
	// 前序遍历
	op := x.Op.String()
	if xRes, ok := (x.LHS).(*BinaryExpr); ok {
		l, err = xRes.esQL(esPtr)
	} else {
		if xRes, ok := (x.LHS).(*ParenExpr); ok {
			l, err = xRes.esQL(esPtr)
		} else {
			l, err = (x.LHS).ESQL()
		}
	}

	if _, regexOk := (x.RHS).(*Regex); regexOk {
		isRegex = true
	}
	if xRes, ok := (x.RHS).(*BinaryExpr); ok {
		r, err = xRes.esQL(esPtr)
	} else {
		if xRes, ok := (x.RHS).(*ParenExpr); ok {
			r, err = xRes.esQL(esPtr)
		} else {
			r, err = (x.RHS).ESQL()
		}
	}

	if err != nil {
		return "", err
	}

	// 获取索引时候的fieldName，
	// 如果使用aliasName，需要转为实际fileName
	if strL, ok := l.(string); ok {
		if k, ok := esPtr.AliasSlice[1][strL]; ok {
			l = k
		}
	}

	// 正则查询
	if isRegex {
		return esPtr.basicTermQuery(QueryFuncs[op], l, r, QueryValue)
	}
	// match query
	if esFunc, ok := r.(*esQlFunc); ok {
		if esFunc.fName == "match" {
			if args, ok := (esFunc.args).([]string); ok {
				return esPtr.basicTermQuery(esFunc.fName, l, args[0], "query")
			}
			return nil, fmt.Errorf("match func params should be field name")

		}
		return nil, fmt.Errorf("unsupport query func: %s", esFunc.fName)
	}
	if v, ok = QueryFuncs[op]; ok && v == "match" {
		return esPtr.basicTermQuery(v, l, r, "query")
	}

	// 逻辑查询(and, or), 即ES DSL中的bool复合查询
	if v, ok = LogicMap[op]; ok {
		return boolCompoundQuery(v, l, r)
	}

	// 逻辑查询(not)，即ES DSL中的must_not查询
	if v, ok = LogicNotMap[op]; ok {
		return esPtr.boolNotQuery(v, l, r)
	}

	// 范围查询, 即ES DSL中的range查询
	if v, ok = ArithmeticMap[op]; ok {
		return rangeTermQuery(v, l, r)
	}

	// 精确值查询, 即ES DSL中的term查询
	if v, ok = TermMap[op]; ok {
		return esPtr.basicTermQuery(v, l, r, QueryValue)
	}

	return "", nil
}

type esQlFunc struct {
	fName string // 函数名称
	// aType string      // 函数参数类型(可变列表/kv对)
	args interface{} // 函数参数(形式归一为 kv对)
}

// ESQL 对FuncExpr结构的解析
func (x *FuncExpr) ESQL() (interface{}, error) {
	var res = new(esQlFunc)

	// match
	if v, ok := QueryFuncs[x.Name]; ok {
		res.fName = v
		var args = []string{}
		for _, i := range x.Param {
			arg, err := i.ESQL()
			if err != nil {
				return nil, err
			}
			strArg, err := voidToString(arg)
			if err != nil {
				return nil, fmt.Errorf("invalid namespace")
			}
			args = append(args, strArg)
		}
		res.args = args
	}

	return res, nil
}

// ESQL target 返回列值
func (x *Target) ESQL() (interface{}, error) {
	if v, ok := x.Col.(*Identifier); ok {
		s, err := v.ESQL()
		if err != nil {
			return "", err
		}
		return s, nil
	}
	return nil, nil

}

// ESQL column
func (x *Identifier) ESQL() (interface{}, error) {
	res := x.String()
	return res, nil
}

// ESQL stringliteral
func (x *StringLiteral) ESQL() (interface{}, error) {
	return x.Val, nil
}

// ESQL orderby
func (x *OrderBy) ESQL() (interface{}, error) {
	return x.String(), nil
}

// ESQL timeexpr
// 返回int64类型的时间戳，单位为毫秒
func (x *TimeExpr) ESQL() (interface{}, error) {
	return int64(x.Time.UnixNano() / int64(time.Millisecond)), nil
}

// ESQL limit
func (x *Limit) ESQL() (interface{}, error) {
	return x.Limit, nil
}

// ESQL slimit
func (x *SLimit) ESQL() (interface{}, error) {
	return x.String(), nil
}

// ESQL offset
func (x *Offset) ESQL() (interface{}, error) {
	return x.String(), nil
}

// ESQL soffset
func (x *SOffset) ESQL() (interface{}, error) {
	return x.String(), nil
}

// ESQL niliteral
func (x *NilLiteral) ESQL() (interface{}, error) {
	return "", fmt.Errorf("nil no support")
}

// ESQL bool
func (x *BoolLiteral) ESQL() (interface{}, error) {
	return fmt.Sprintf("%v", x.Val), nil
}

// ESQL regex
func (x *Regex) ESQL() (interface{}, error) {
	return x.Regex, nil
}

// ESQL numberliteral
func (x *NumberLiteral) ESQL() (interface{}, error) {
	return x.String(), nil
}

// ESQL funcarg
func (x *FuncArg) ESQL() (interface{}, error) {
	return "", fmt.Errorf("not impl")
}

// ESQL fill
func (x *Fill) ESQL() (interface{}, error) {
	return "", fmt.Errorf("not impl")
}

// ESQL nodelist
func (x NodeList) ESQL() (interface{}, error) {
	return "", nil
}

// ESQL timezone
func (x *TimeZone) ESQL() (interface{}, error) {
	return "", nil
}

// ESQL timerange
func (x *TimeRange) ESQL() (interface{}, error) {
	return "", nil
}

// esQL parenexpr
func (x *ParenExpr) esQL(esPtr *EST) (interface{}, error) {
	if xRes, ok := (x.Param).(*BinaryExpr); ok {
		return xRes.esQL(esPtr)
	}
	return x.Param.ESQL()
}

// ESQL parenexpr
func (x *ParenExpr) ESQL() (interface{}, error) {
	return nil, nil
}

// ESQL stmts
func (x Stmts) ESQL() (interface{}, error) {
	return "", nil
}

// ESQL FuncArgList
func (x FuncArgList) ESQL() (interface{}, error) {
	return "", fmt.Errorf("not impl")
}

// ESQL Star
func (x *Star) ESQL() (interface{}, error) {
	return "", fmt.Errorf("not impl")
}

// ESQL AttrExpr
func (x *AttrExpr) ESQL() (interface{}, error) {
	return "", fmt.Errorf("not impl")
}

// ESQL CascadeFunctions
func (x *CascadeFunctions) ESQL() (interface{}, error) {
	return "", fmt.Errorf("not impl")
}

// ESQL TimeResolution
func (x *TimeResolution) ESQL() (interface{}, error) {
	return "", fmt.Errorf("not impl")
}

// ESQL Lambda
func (x *Lambda) ESQL() (interface{}, error) {
	return "", fmt.Errorf("not impl")
}

// ESQL StaticCast
func (x *StaticCast) ESQL() (interface{}, error) {
	return "", fmt.Errorf("not impl")
}
