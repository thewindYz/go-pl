package es

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/olivere/elastic/v7"
)

type EsCli struct {
	Es      *elastic.Client
	TimeOut string
}

var MaxBulkActions = 1000

func InitEsCli(host, user, pwd, timeout string) (*EsCli, error) {
	es, err := InitEs(host, user, pwd)
	if err != nil {
		return nil, err
	}
	return &EsCli{Es: es, TimeOut: timeout}, nil
}

func InitEs(host string, user, pwd string) (*elastic.Client, error) {

	client, err := elastic.NewClient(
		elastic.SetURL(host),
		elastic.SetSniff(false), //disable sniffing
		elastic.SetBasicAuth(user, pwd),
	)
	if err != nil {
		// Handle error
		l.Errorf("NewClient %s", err.Error())
		return nil, err
	}

	ctx := context.Background()
	info, code, err := client.Ping(host).Do(ctx)
	if err != nil {
		l.Errorf("Ping %s", err.Error())
		return nil, err
	}

	l.Infof("Elasticsearch returned with code %d and version %s\n", code, info.Version.Number)

	return client, nil
}

func (cli *EsCli) IndexExists(indexName string) (bool, error) {
	exists, err := cli.Es.IndexExists(indexName).Do(context.Background())
	if err != nil {
		l.Errorf("IndexExists %s", err.Error())
		return false, err
	}
	return exists, nil
}

func (cli *EsCli) CreateIndex(indexName, tableMapping string) error {
	exists, err := cli.Es.IndexExists(indexName).Do(context.Background())
	if err != nil {
		l.Errorf("IndexExists %s", err.Error())
		return err
	}

	if exists {
		return nil
	}

	res, err := cli.Es.CreateIndex(indexName).Timeout(cli.TimeOut).Body(tableMapping).Do(context.Background())
	if err != nil {
		l.Errorf("CreateIndex %s", err.Error())
		return err
	}

	if !res.Acknowledged {
		l.Warnf("[warn] create index %s not acknowledged", indexName)
	}

	return nil
}

func (cli *EsCli) PutPipeline(id string, body string) error {
	res, err := cli.Es.IngestPutPipeline(id).Timeout(cli.TimeOut).BodyString(body).Do(context.Background())
	if err != nil {
		l.Errorf("IngestPutPipeline %s", err.Error())
		return err
	}

	l.Debugf(" %+#v", res)
	return nil
}

type WriteEsBody struct {
	Id        string                 `json:"_id"`
	IndexName string                 `json:"index_name"`
	ContMd5   string                 `json:"content_md5"`
	Content   map[string]interface{} `json:"content"`
	IsUpdate  bool                   `json:"is_update"`
}

func (cli *EsCli) WriteBulk(pipeLineId, indexName string, bs []*WriteEsBody) ([]int, []int, error) {

	fails := []int{}
	sucs := []int{}
	start := time.Now()
	bulkService := cli.Es.Bulk()

	bulkService.Timeout(cli.TimeOut)

	if pipeLineId != "" {
		bulkService.Pipeline(pipeLineId)
	}

	for _, b := range bs {
		index := ``
		if b.IndexName != "" {
			index = b.IndexName
		} else {
			index = indexName
		}

		var r *elastic.BulkIndexRequest
		//更新操作

		if b.IsUpdate {
			bulkService.Add(elastic.NewBulkUpdateRequest().Index(index).Id(b.Id).Doc(b.Content))
		} else {

			if b.Id != "" {
				r = elastic.NewBulkIndexRequest().Index(index).Id(b.Id).Doc(b.Content)
			} else {
				r = elastic.NewBulkIndexRequest().Index(index).Doc(b.Content)
			}

			bulkService.Add(r)
		}

	}

	// 写入ES
	if bulkService.NumberOfActions() > 0 {
		resp, err := bulkService.Do(context.TODO())

		if err != nil {
			l.Errorf("%s", err.Error())
			return nil, nil, err
		}

		if resp.Errors {
			for index, item := range resp.Items {

				if item[`index`] == (*elastic.BulkResponseItem)(nil) {
					continue
				}

				l.Errorf("%+#v, err:%+#v", item[`index`], item[`index`].Error)

				switch item[`index`].Status / 100 {
				case 2:
					sucs = append(sucs, index)
				case 4:
					// cluster_block_exception
					if strings.Contains(item[`index`].Error.Reason, `index read-only`) {
						fails = append(fails, index)
						continue
					}
					l.Warnf("es write status %d", item[`index`].Status)
				default:
					if item[`index`].Error != nil {
						fails = append(fails, index)
					}

				}
			}
		} else {
			for index := range resp.Items {
				sucs = append(sucs, index)
			}
		}
	}

	l.Infof("es writed %s/%d, time consuming %v", indexName, len(bs)-len(fails), time.Since(start))
	return sucs, fails, nil
}

func (cli *EsCli) QueryTerms(indexName, name, className string) (map[string]interface{}, error) {
	q := elastic.NewBoolQuery()
	q.Must(elastic.NewTermQuery("__name.keyword", name))
	q.Must(elastic.NewTermQuery(`__class.keyword`, className))

	resp, err := cli.Es.Search(indexName).Timeout(cli.TimeOut).Query(q).Do(context.Background())
	if err != nil {
		l.Errorf("%s", err.Error())
		return nil, err
	}

	if int(resp.Hits.TotalHits.Value) <= 0 {
		l.Debugf("count %v", resp.Hits.TotalHits.Value)
		return nil, nil
	}

	item := map[string]interface{}{}
	for _, h := range resp.Hits.Hits {

		err := json.Unmarshal(h.Source, &item)
		if err != nil {
			l.Errorf("%s", err.Error())
			return nil, err
		}
		// l.Debugf("%+#v", item)
		item[`_id`] = h.Id
	}
	return item, nil

}

func (cli *EsCli) QueryKeyevent(indexName string, eventUIDs []string) ([]WriteEsBody, error) {
	q := elastic.NewBoolQuery()
	q.Must(elastic.NewTermsSetQuery(`__tags.__eventId.keyword`, eventUIDs))

	aggTop := elastic.NewTopHitsAggregation().Sort(`__timestampUs`, false).Size(1)

	aggs := elastic.NewTermsAggregation().Field(`__tags.__eventId.keyword`)
	for _, event := range eventUIDs {
		aggs.IncludeValues(event)
	}
	aggs.SubAggregation(`topTimeStamp`, aggTop)

	resp, err := cli.Es.Search(indexName).Timeout(cli.TimeOut).Aggregation(`eventId`, aggs).Size(0).Do(context.Background())
	if err != nil {
		l.Errorf("%s", err.Error())
		return nil, err
	}

	termRes, _ := resp.Aggregations.Terms(`eventId`)
	res := []WriteEsBody{}

	if termRes != nil {
		for _, bucket := range termRes.Buckets {
			aggRes, _ := bucket.Aggregations.TopHits(`topTimeStamp`)
			item := map[string]interface{}{}
			itemID := ``
			if aggRes != nil {
				for _, h := range aggRes.Hits.Hits {
					err := json.Unmarshal(h.Source, &item)
					if err != nil {
						l.Errorf("%s", err.Error())
						return nil, err
					}
					itemID = h.Id
				}
			}

			res = append(res, WriteEsBody{
				Id:      itemID,
				Content: item,
			})
		}

	}

	return res, nil

}

func (cli *EsCli) DeleteObjects(wsUUID string, ts int64) error {
	q := elastic.NewBoolQuery()
	q.Must(elastic.NewTermQuery("__workspaceUUID", wsUUID))
	q.Filter(elastic.NewRangeQuery("__esLastUpdateTime").Lt(ts))

	_, err := elastic.NewDeleteByQueryService(cli.Es).Timeout(cli.TimeOut).Query(q).Do(context.Background())
	if err != nil {
		l.Errorf("%s", err.Error())
		return err
	}

	return nil

}

func (cli *EsCli) UpdateByQueryObjects(class, index string, script string, params map[string]interface{}) error {
	q := elastic.NewBoolQuery()
	q.Must(elastic.NewTermQuery("__class", class))

	s := elastic.NewScript(script)
	for k, v := range params {
		s.Param(k, v)
	}

	_, err := elastic.NewUpdateByQueryService(cli.Es).Index(index).Timeout(cli.TimeOut).Query(q).Script(s).Do(context.Background())
	if err != nil {
		l.Errorf("%s", err.Error())
		return err
	}

	return nil
}

func (cli *EsCli) DocExist(indexName, docId string) (bool, error) {
	exists, err := cli.Es.Exists().Index(indexName).Id(docId).Do(context.Background())
	if err != nil {
		l.Errorf("Doc Exists %s", err.Error())
		return false, err
	}
	return exists, nil
}

func (cli *EsCli) CatIndicesCounts(indexName string, target map[string]int) error {

	var res elastic.CatIndicesResponse
	var err error

	catS := elastic.NewCatIndicesService(cli.Es)

	if len(indexName) > 0 {
		res, err = catS.Index(indexName).Do(context.Background())
	} else {
		res, err = catS.Do(context.Background())
	}

	if err != nil {
		l.Errorf("cat indices %s", err.Error())
		return err
	}

	for _, r := range res {

		indexStrs := strings.Split(r.Index, `-`)

		if _, ok := target[indexStrs[0]]; !ok {
			target[indexStrs[0]] = r.DocsCount
		} else {
			target[indexStrs[0]] += r.DocsCount
		}
	}

	return nil
}

// ShowColumnsRes x-pack sql show columns查询返回结构
// {
// 	"columns" : [
// 	  {
// 		"name" : "column",
// 		"type" : "keyword"
// 	  },
// 	  {
// 		"name" : "type",
// 		"type" : "keyword"
// 	  },
// 	  {
// 		"name" : "mapping",
// 		"type" : "keyword"
// 	  }
// 	],
// 	"rows" : [
// 	  [
// 		"__content",
// 		"VARCHAR",
// 		"keyword"
// 	  ]
// 	]
// }
type ShowColumnsRes struct {
	Columns interface{} `json:"columns"` // 不使用该字段
	Rows    [][]string  `json:"rows"`
}

// XPackSQL x-pack sql查询
func (cli *EsCli) XPackSQL(indexName string) ([][]string, error) {

	body := map[string]string{
		"query": "SHOW COLUMNS IN " + indexName,
	}
	pRequestOptions := elastic.PerformRequestOptions{
		Method: "GET",
		Path:   "/_sql?format=json", // build url
		Body:   body,
	}
	res, err := cli.Es.PerformRequest(context.Background(), pRequestOptions)
	if err != nil { // 查询报错
		return nil, err
	}
	ret := new(ShowColumnsRes)
	if err := json.Unmarshal(res.Body, ret); err != nil {
		return nil, err
	}
	return ret.Rows, nil
}

// MSearchFieldExists fields存在查询，用于show fields
func (cli *EsCli) MSearchFieldExists(indexName, classDQL string, rows [][]string) ([][]string, error) {
	//
	var esReqs = []*elastic.SearchRequest{}
	// var dqls = []string{}
	for _, row := range rows {
		// 构造exists查询
		// classDQL := `{"term":{"type":{"value":"resource"}}}`
		dql := fmt.Sprintf(
			`{"size":0,"query":{"bool":{"must":[%s,{"exists":{"field":"%s"}}]}}}`,
			classDQL,
			row[0],
		)
		esReq := elastic.NewSearchRequest().Timeout(cli.TimeOut).Index(indexName).Source(dql)
		esReqs = append(esReqs, esReq)
	}

	results, err := cli.Es.MultiSearch().Add(esReqs...).Do(context.Background())
	if err != nil {
		return nil, err
	}
	log.Println("--msearch--")
	ss, _ := json.Marshal(results)
	log.Println(string(ss))
	log.Println(err)
	res := [][]string{}
	// 解析结果
	// Responses []*SearchResult *SearchHits
	if results.Responses != nil {
		for i, sPtr := range results.Responses {
			// 存在结果
			if sPtr != nil && sPtr.Hits != nil && sPtr.Hits.TotalHits != nil && sPtr.Hits.TotalHits.Value > 0 {
				res = append(res, rows[i])
			}
		}
	}

	return res, nil
}
