&nbsp;

## 一、名词

&nbsp;

- `M` - 指时序数据中的指标集
- `L` - 日志数据，以字段 `__source` 作为逻辑意义上的分类
- `O` - 对象数据，以字段 `__class` 作为逻辑意义上的分类
- `E` - 事件数据，以字段 `__source` 作为逻辑意义上的分类
- `T` - 追踪数据，以字段 `__serviceName` 作为逻辑意义上的分类
- `R` - RUM 数据，以字段 `type` 作为逻辑意义上的分类

&nbsp;

## 二、show 函数列表

&nbsp;

---

&nbsp;

### 2.1 show_object_class

&nbsp;

展示对象数据的指标集合

&nbsp;

---

&nbsp;

## 三、函数列表

&nbsp;

&nbsp;

### 3.1 avg

&nbsp;

返回字段的平均值

&nbsp;

(1) `avg(field1)`

参数有且只有一个，参数类型是字符串，参数值为字段名称

适用范围: `M`, `L`, `O`, `E`, `T`, `R`

&nbsp;

---

&nbsp;

### 3.2 bottom

&nbsp;

返回最小的 n 个 field 值

&nbsp;

(1) `bottom(field1, n)`

参数有且只有两个，

第一个参数表示字段名称，类型为字符串

第二个参数表示返回数量，类型为数值

适用范围: `M`, `L`, `O`, `E`, `T`, `R`

&nbsp;

---

&nbsp;

### 3.3 count

&nbsp;

返回非空字段值的汇总值

&nbsp;

(1) `count(field1)`

参数有且只有一个，参数类型是字符串，参数值为字段名称

适用范围: `M`, `L`, `O`, `E`, `T`, `R`

&nbsp;

(2) `count(func1())`

参数可以是一个内置函数，例如: `count(distinct(field1))`

适用范围: `M`

&nbsp;

---

&nbsp;

## 3.4 derivative

&nbsp;

返回字段的相邻两个点的变化率

&nbsp;

(1) `derivative(field1)`

参数有且只有一个，参数类型是字符串，参数值为字段名称

适用范围: `M`

注: `field1`对应值为数值类型

&nbsp;

---

&nbsp;

## 3.5 difference

&nbsp;

差值

&nbsp;

适用范围: `M`

&nbsp;

---

&nbsp;

## 3.6 distinct

&nbsp;

返回`field value`的不同值列表

&nbsp;

(1) `distinct(field1)`

参数有且只有一个，参数类型是字符串，参数值为字段名称

适用范围: `M`, `L`, `O`, `E`, `T`, `R`

&nbsp;

---

&nbsp;

## 3.7 first

&nbsp;

返回时间戳最早的值

&nbsp;

(1) `first(field1)`

参数有且只有一个，参数类型是字符串，参数值为字段名称

适用范围: `M`, `L`, `O`, `E`, `T`, `R`

&nbsp;

---

&nbsp;

## 3.8 last

&nbsp;

返回时间戳最近的值

&nbsp;

(1) `last(field1)`

参数有且只有一个，参数类型是字符串，参数值为字段名称

适用范围: `M`, `L`, `O`, `E`, `T`, `R`

&nbsp;

---

&nbsp;

## 3.9 log

&nbsp;

求对数

&nbsp;

适用范围: `M`

&nbsp;

---

&nbsp;

## 3.10 match

&nbsp;

全文搜索（模糊搜索）

&nbsp;

&nbsp;

(1) `field1=match(field1_value)`

参数有且只有一个, 表示查询的字段值

适用范围: `L`, `O`, `E`, `T`, `R`

示例:

```shell
# 注意, match函数位于filter-clause语句中

rum::page:(){crash_stack=match(`crash_stack_29UK2S4NTa`)}
```

&nbsp;

---

&nbsp;

## 3.11 max

&nbsp;

返回最大的字段值

&nbsp;

&nbsp;

(1) `max(field1)`

参数有且只有一个，参数类型是字符串，参数值为字段名称

适用范围: `M`, `L`, `O`, `E`, `T`, `R`

&nbsp;

---

&nbsp;

## 3.12 min

&nbsp;

返回最小的字段值

&nbsp;

&nbsp;

(1) `min(field1)`

参数有且只有一个，参数类型是字符串，参数值为字段名称

适用范围: `M`, `L`, `O`, `E`, `T`, `R`

&nbsp;

---

&nbsp;

## 3.13 moving_average

&nbsp;

平均移动

&nbsp;

适用范围: `M`

&nbsp;

---

&nbsp;

## 3.14 non_degative_derivative

&nbsp;

数据的非负变化率

&nbsp;

适用范围: `M`

&nbsp;

---

&nbsp;

## 3.15 percent

&nbsp;

返回较大百分之 n 的字段值

&nbsp;

适用范围: `M`, `L`, `O`, `E`, `T`, `R`

&nbsp;

---

&nbsp;

## 3.16 re

&nbsp;

查询时候，正则过滤

&nbsp;

&nbsp;

(1) `field1=re(field1_value)`

参数有且只有一个, 表示查询的字段值

适用范围: `M`, `L`, `O`, `E`, `T`, `R`

示例:

```shell
# 注意, re函数位于filter-clause语句中

rum::page:(){crash_stack=re(`.*_29UK2S4NTa`)}
```

&nbsp;

---

&nbsp;

## 3.17 sum

&nbsp;

返回字段值的和

&nbsp;

&nbsp;

(1) `sum(field1)`

参数有且只有一个，参数类型是字符串，参数值为字段名称

适用范围: `M`, `L`, `O`, `E`, `T`, `R`

&nbsp;

---

&nbsp;

## 3.18 top

&nbsp;

返回最大的 n 个 field 值

&nbsp;

&nbsp;

(1) `top(field1, n)`

参数有且只有两个，

第一个参数表示字段名称，类型为字符串

第二个参数表示返回数量，类型为数值

适用范围: `M`, `L`, `O`, `E`, `T`, `R`

&nbsp;
