package dql

import (
	"bytes"
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/influxdata/influxdb1-client/models"
	influxdb "github.com/influxdata/influxdb1-client/v2"

	uhttp "gitlab.jiagouyun.com/cloudcare-tools/cliutils/network/http"
	"gitlab.jiagouyun.com/cloudcare-tools/kodo/config"
	"gitlab.jiagouyun.com/cloudcare-tools/kodo/ctrl"
	"gitlab.jiagouyun.com/cloudcare-tools/kodo/dql/parser"
	"gitlab.jiagouyun.com/cloudcare-tools/kodo/es"
	"gitlab.jiagouyun.com/cloudcare-tools/kodo/rtpanic"
)

type queryWorker struct {
	idx         int
	influxClis  map[string]*influxQueryCli // cache influx-instance-id -> influx-client
	influxDBs   map[string][]string        // cache workspace-id -> influx-dbname,influx-instance-id
	esCli       *es.EsCli
	curRewriter *Rewrite
}

type DQLResults struct {
	Ses []*QueryResult
	Msg string
}

func (qw *queryWorker) getInfluxQueryCli(wsid string) (*influxQueryCli, error) {
	var influxID string

	if _, ok := qw.influxDBs[wsid]; !ok {

		ifdb, err := ctrl.QueryWorkspaceInfoReadWrite(wsid) // query influx instance info of @wsid

		if err != nil {
			l.Errorf("load dbinfo(uuid: %s) failed: %s", wsid, err.Error())
			return nil, err
		}

		influxID = ifdb.InfluxInstanceUUID

		qw.influxDBs[wsid] = []string{ifdb.DB, influxID}

		if _, ok := qw.influxClis[influxID]; !ok { // instance not connected before
			cli, err := influxdb.NewHTTPClient(influxdb.HTTPConfig{ // create new influx client
				Addr:               ifdb.Host,
				Username:           ifdb.User,
				Password:           ifdb.Pwd,
				UserAgent:          "dql-query",
				Timeout:            time.Duration(config.C.Influx.ReadTimeOut) * time.Second,
				InsecureSkipVerify: true,
			})

			if err != nil {
				l.Error(err)
				return nil, err
			}

			qw.influxClis[influxID] = &influxQueryCli{cli: cli}

			l.Debugf("[%d] new client %s:%+#v", qw.idx, influxID, qw.influxClis[influxID])
		}
	}

	return qw.influxClis[qw.influxDBs[wsid][1]], nil
}

func (qw *queryWorker) run(idx int) {

	l.Debugf("query worker %d started.", idx)

	var err error
	qw.esCli, err = es.InitEsCli(config.C.Es.Host,
		config.C.Es.User,
		config.C.Es.Passwd,
		config.C.Es.TimeOut)

	if err != nil {
		l.Errorf("InitEs(): %s", err.Error())
		return
	}

	var f rtpanic.RecoverCallback

	var lastIQ *InnerQuery
	panicCnt := 0

	f = func(trace []byte, err error) {
		defer rtpanic.Recover(f, nil)

		if trace != nil || err != nil {
			panicCnt++
			l.Warnf("[%d] recoverd from panic. panic: %v\nstack trace:\n%s", panicCnt, err, string(bytes.TrimSpace(trace)))
			if lastIQ != nil {
				select {
				case lastIQ.result <- uhttp.Errorf(ErrQueryWorkerCrashed, "trace: %s", string(trace)):
				default:
				}
			}
		}

		for {
		start:
			select {
			case iq := <-qch:

				lastIQ = iq

				l.Debugf("query: %+#v", iq)

				var response []*QueryResult

				for _, query := range iq.Queries {
					pres, err := qw.parseQuery(query)
					if err != nil {
						iq.result <- uhttp.Errorf(ErrParseError, "parse error: %s", err.Error())
						goto start
					}

					results, err := qw.runQuery(iq, pres)
					if err != nil {
						iq.result <- uhttp.Errorf(ErrQueryError, "query error: %s", err.Error())
						goto start
					}

					response = append(response, results...)

				}

				iq.result <- response
			}
		}
	}

	f(nil, nil)
}

func (qw *queryWorker) parseQuery(sq *singleQuery) (ASTResults, error) {
	qw.curRewriter = &Rewrite{
		TimeRange:   sq.TimeRange,
		MaxPoint:    sq.MaxPoint,
		MaxDuration: strings.TrimSpace(sq.MaxDuration),
		Conditions:  strings.TrimSpace(sq.Conditions),
	}

	parseResults, err := Parse(sq.Query, qw.curRewriter)
	if err != nil {
		l.Error(err)
		return nil, err
	}

	if len(parseResults) > maxDQLParseResult {
		return nil, fmt.Errorf("do not query larger than %d", maxDQLParseResult)
	}

	return parseResults, nil
}

func (qw *queryWorker) runQuery(iq *InnerQuery, res ASTResults) ([]*QueryResult, error) {
	var datas []*QueryResult

	for _, ast := range res {

		data, err := qw.runSingleQuery(iq.WorkspaceUUID, ast, iq.EchoExplain)
		if err != nil {
			return nil, err
		}

		if data == nil {
			l.Debugf("got no data on query: %s", ast.Q)
			continue
		}

		switch ast.AST.(type) {
		case *parser.DFQuery:

			err := FillResult(ast.AST.(*parser.DFQuery), data.Series)
			if err != nil {
				return nil, err
			}
		}

		datas = append(datas, data)
	}

	return datas, nil
}

func (qw *queryWorker) runSingleQuery(wsid string, ast *ASTResult, explain bool) (*QueryResult, error) {
	l.Debugf("[%d] namespace: %s, query: %s", qw.idx, ast.Namespace, ast.Q)

	switch ast.Namespace {
	case NSMetric:
		qcli, err := qw.getInfluxQueryCli(wsid)
		if err != nil {
			return nil, err
		}
		if qcli == nil {
			l.Fatalf("[%d] qcli nil: %+#v", qw.idx, qw.influxClis)
		}

		qr, err := qw.query(ast.Q, qw.influxDBs[wsid][0], DefaultRP, InfluxReadPrecision, qcli)
		if err != nil {
			return nil, err
		}
		if explain {
			qr.RawQuery = ast.Q
		}

		// qrs = append(qrs, qr)
		return qr, nil

	case NSObject, NSLogging, NSEvent, NSTracing, NSRum:
		qrs, err := qw.esQuery(ast, wsid, explain)
		if err != nil {
			return nil, err
		}

		return qrs, nil

	case NSFunc, NSFuncAbbr:

		qr, err := qw.funcQuery(wsid, ast, explain)
		if err != nil {
			return nil, err
		}
		// qrs = append(qrs, qr)
		return qr, nil

	case NSLambda:
		return qw.lambdaQuery(wsid, ast, explain)

	}

	return nil, fmt.Errorf("invalid namespace `%s'", ast.Namespace)
}

type influxQueryCli struct {
	cli influxdb.Client
}

// type QueryResult struct {
// 	Results []influxdb.Result `json:"results"`
// 	ErrMsg  string            `json:"errMsg"`
// }

func (qw *queryWorker) query(q, db, rp, precision string, c *influxQueryCli) (*QueryResult, error) {
	influxq := influxdb.Query{
		Command:         q,
		Database:        db,
		RetentionPolicy: rp,
		Precision:       precision,
		Parameters:      nil,
		Chunked:         false,
		ChunkSize:       0,
	}

	start := time.Now()

	res, err := c.cli.Query(influxq)
	if err != nil {
		l.Errorf("influxdb api error %s", err.Error())
		return nil, nil
	}

	elapsed := time.Since(start)

	ses := []models.Row{}
	for _, r := range res.Results {
		ses = append(ses, r.Series...)
	}

	return &QueryResult{
		Series: ses,
		Cost:   fmt.Sprintf("%v", elapsed),
	}, nil
}

// ES: query & convert ES result -> Series
func (qw *queryWorker) esQuery(ast *ASTResult,
	wsID string,
	explain bool) (*QueryResult, error) {

	var qres *QueryResult // influx结构化后返回值

	indexName := ``
	switch ast.Namespace {
	case `object`, `O`:
		indexName = wsID + `_object`
	case `logging`, `L`:
		indexName = wsID + `_logging`
	case `keyevent`, `E`, `event`:
		indexName = wsID + `_keyevent`
	case `tracing`, `T`:
		indexName = wsID + `_tracing`
	case `rum`, `R`:
		indexName = wsID + `_rum`
	default:
		l.Errorf(`No Support`)
		return nil, fmt.Errorf("no support namespace")
	}

	showAst, ok := ast.AST.(*parser.Show)
	if ok && showAst.Helper.ESTResPtr.ShowFields { // 满足show函数 且是 show fields
		res, err := qw.esCli.XPackSQL(indexName)
		if err != nil {
			return nil, err
		}
		existRes, err := qw.esCli.MSearchFieldExists(indexName, ast.Q, res)
		if err != nil {
			return nil, err
		}
		qres, err = esShowColumnsToInflux(existRes, indexName)
	} else {
		esRes, err := qw.esCli.Es.Search().
			Timeout(qw.esCli.TimeOut).
			Index(indexName).
			Source(ast.Q).
			Do(context.Background())

		if err != nil {
			l.Errorf(`dql:%s, indexName:%s, %s`, ast.Q, indexName, err.Error())
			return nil, err
		}

		if esRes.Error != nil {
			l.Error(`ES query result error: %s`, esRes.Error)
			return nil, fmt.Errorf(esRes.Error.Reason)
		}

		qres, err = es2influx(esRes, ast)
		if err != nil {
			return nil, err
		}
	}

	if explain {
		qres.RawQuery = ast.Q
	}

	return qres, nil
}
