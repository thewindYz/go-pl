package main

import (
	"fmt"
)

func f1(strs ...string) {
	fmt.Println("----f1----")
}

func main() {
	// var esReqs = []string{}
	// f1(esReqs...)
	var influxqls [][]string
	fmt.Println(influxqls)
	fmt.Println(len(influxqls))
	var ifxql []string
	influxqls = append(influxqls, ifxql)
	fmt.Println(len(influxqls))
	fmt.Println(influxqls)

	// var sptr = new([]string)
	// fmt.Println(sptr)
	// var s = []string{}
	// *sptr = s
	// ss, _ := json.Marshal(s)
	// fmt.Printf("%s\n", string(ss))
	// fmt.Println(sptr)
}
