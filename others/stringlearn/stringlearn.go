package main

import (
	"fmt"
	"strings"
)

func main() {
	s := strings.Join([]string{"a", "b", "c"}, "-")
	fmt.Println(s)
}
