package main

import "fmt"

func main() {

	fmt.Println("--enter main func--")

	defer func() {

		fmt.Println("--enter defer func--")

		p := recover()

		if p != nil {

			fmt.Printf("panic: %s \n", p)
		}

		fmt.Println("--defer func end--")

	}()

	panic("test panic")

	fmt.Println("--main func end--")

}
