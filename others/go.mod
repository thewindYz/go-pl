module golearn.com/others

go 1.13

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/google/go-cmp v0.5.3
	github.com/influxdata/influxdb1-client v0.0.0-20200827194710-b269163b24ab
	github.com/olivere/elastic/v7 v7.0.22
	gitlab.jiagouyun.com/cloudcare-tools/cliutils v0.0.0-20201106065711-fd845b00af60
	gitlab.jiagouyun.com/cloudcare-tools/kodo v0.0.0-20201224090647-3562198c601c
)
