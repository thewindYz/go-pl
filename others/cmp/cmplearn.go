package main

import (
	"fmt"

	"github.com/google/go-cmp/cmp"
)

func main() {
	s := cmp.Diff("hello world", "world")
	fmt.Println(s)
}
