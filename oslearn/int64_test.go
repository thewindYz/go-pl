package oslearn

import (
	"encoding/json"
	"fmt"
	"math"
	"testing"
)

type KeepZero float64

// func (f KeepZero) MarshalJSON() ([]byte, error) {
// 	if float64(f) == float64(int(f)) {
// 		return []byte(strconv.FormatFloat(float64(f), 'f', 1, 32)), nil
// 	}
// 	return []byte(strconv.FormatFloat(float64(f), 'f', -1, 32)), nil
// }

func (f KeepZero) MarshalJSON() ([]byte, error) {
	tf := math.Trunc(float64(f))
	fmt.Println("--tf--", tf)
	if float64(f) == tf {
		fmt.Println("---float--")
		return []byte(fmt.Sprintf("%.1f", f)), nil
	}
	fmt.Println("---int--")
	return []byte(fmt.Sprintf("%f", f)), nil
}

type Pt struct {
	Value KeepZero
	Unit  string
}

func KeepZeroMap(source map[string]interface{}) map[string]interface{} {
	for k, v := range source {
		switch v.(type) {
		case float64:
			source[k] = KeepZero(v.(float64))
		case float32:
			source[k] = KeepZero(v.(float32))
		case map[string]interface{}:
			source[k] = KeepZeroMap(v.(map[string]interface{}))
		default:

		}
	}

	return source
}

func TestFloat64(t *testing.T) {

	var a float32 = 100
	var b float64 = 200.0
	var c interface{} = 1.61233960431e+12

	ctx := map[string]interface{}{
		"a": a,
		"b": b,
		"c": c,
	}

	content := KeepZeroMap(ctx)
	data, err := json.Marshal(content)
	fmt.Println(string(data), err)
}

func TestDefault(t *testing.T) {
	var a float32 = 100
	var b float64 = 200.0
	var c interface{} = 100

	ctx := map[string]interface{}{
		"a": a,
		"b": b,
		"c": c,
	}

	data, err := json.Marshal(ctx)
	fmt.Println(string(data), err)

}

func TestUnmarshal(t *testing.T) {
	m := map[string]interface{}{
		"id":   1612339604310,
		"Name": "Fernando",
	}

	result, _ := json.Marshal(m)

	fmt.Println(string(result))

	var f interface{}
	json.Unmarshal(result, &f)

	fmt.Println(f)

}
