package oslearn

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"testing"
)

func TestRename(t *testing.T) {
	filePath := "/Users/yz/work/gitee/go-pl/111.md"
	nFilePath := "/Users/yz/work/gitee/go-pl/222.md"
	os.Rename(filePath, nFilePath)
}

func TestCreateTemp(t *testing.T) {
	dir, err := ioutil.TempDir("/Users/yz/work/gitee/go-pl/", "prefix")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(dir, err)
}

func TestReadByte(t *testing.T) {
	filePath := "/Users/yz/work/github/lucene1.0.1/src/java/index/s1.fnm"
	f, _ := os.Open(filePath)

	// b := make([]byte, 20)
	// f.Read(b)
	// fmt.Println(b)

	i := ReadVarInt64(f)

	for i > 0 {
		s := ReadString(f)
		fmt.Println(s)
		i = i - 1
	}
	fmt.Println("----end----")

}

func ReadVarInt64(f *os.File) int64 {
	var (
		i, shift int64
		b        byte
		err      error
	)
	b1 := make([]byte, 1)
	_, err = f.Read(b1)
	if err != nil {
		fmt.Println(err.Error())
	}

	b = b1[0]
	i = int64(b) & 0x7F

	shift = 7

	for int64(b)&0x80 != 0 {

		_, err = f.Read(b1)
		b = b1[0]
		i = i | (int64(b)&0x7F)<<shift
		shift = shift + 7
	}
	fmt.Println(i)
	return i
	// return i, nil

}

func ReadString(f *os.File) string {
	var (
		n   int64
		err error
	)
	n = ReadVarInt64(f) // 先读取字符串的长度
	if n < 1 {
		n = 1
	}
	b := make([]byte, n)
	_, err = f.Read(b)
	if err != nil {
		return ""
	}
	return string(b)
}

// func TestReadByte(t *testing.T) {

// }
