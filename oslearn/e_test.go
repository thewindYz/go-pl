package oslearn

import (
	"fmt"
	"os"
	"os/exec"
)

// 根据err type，进行不同的处理
func underlyingError(err error) error {

	switch err := err.(type) {

	case *os.PathError:
		return err.Err

	case *os.LinkError:
		return err.Err

	case *os.SyscallError:
		return err.Err

	case *exec.Error:
		return err.Err

	}
	return err

}

// print error
func printError(i int, err error) {

	if err == nil {
		fmt.Println("nil error")
		return
	}

	err = underlyingError(err)

	// 判断err为具体值
	switch err {

	case os.ErrClosed:
		fmt.Printf("error(closed)[%d]: %s\n", i, err)

	case os.ErrInvalid:
		fmt.Printf("error(invalid)[%d]: %s\n", i, err)

	case os.ErrPermission:
		fmt.Printf("error(permission)[%d]: %s\n", i, err)
	}
}
