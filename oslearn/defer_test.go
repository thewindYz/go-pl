package oslearn

import (
	"fmt"
	"os"
	"testing"
)

func TestDefer(t *testing.T) {
	f := createFile("/tmp/t1.md")
	defer closeFile(f)
	writeFile(f)
}

func createFile(filePath string) *os.File {
	fmt.Println("--create file--")
	f, err := os.Create(filePath)
	if err != nil {
		panic(err)
	}
	return f
}

func writeFile(f *os.File) {
	fmt.Println("writing")
	fmt.Fprintln(f, "data")
}

func closeFile(f *os.File) {
	fmt.Println("--close file--")
	err := f.Close()
	if err != nil {
		fmt.Fprintf(os.Stderr, "error : %v \n", err)
		os.Exit(1)
	}
}
