package oslearn

import (
	"encoding/binary"
	"fmt"
	"log"
	"os"
	"strconv"
	"testing"
)

func TestReadFile(t *testing.T) {
	log.Println("--start--")
	filePath1 := "/tmp/test.tis"
	f1, err := os.Open(filePath1)

	if err != nil {
		log.Fatal(err)
	}
	b1 := make([]byte, 10)
	n1, err := f1.Read(b1)

	ss := []byte("path")
	fmt.Printf("%d bytes: %s\n", n1, string(b1[:n1]))
	fmt.Printf(string(ss))

}

func TestReadFileDiff(t *testing.T) {
	log.Println("--start--")
	filePath1 := "/Users/yz/work/github/lucene1.0.1/src/java/index1/_1.frq"
	// filePath1 := "/Users/yz/work/github/lucene1.0.1/src/java/index/s1.fnm"
	f1, err := os.Open(filePath1)

	if err != nil {
		log.Fatal(err)
	}
	b1 := make([]byte, 100)
	n1, err := f1.Read(b1)

	filePath2 := "/Users/yz/work/github/gsearch/test/index1/_1.frq"
	// filePath2 := "/Users/yz/work/github/gsearch/test/index/s1.fnm"
	f2, err := os.Open(filePath2)

	if err != nil {
		log.Fatal(err)
	}
	b2 := make([]byte, 100)
	n2, err := f2.Read(b2)

	ss := []byte("path")
	fmt.Printf("%d bytes: %s\n", n1, string(b1[:n1]))
	fmt.Printf("%d bytes: %s\n", n2, string(b2[:n2]))
	fmt.Printf(string(ss))

}

func TestLen(t *testing.T) {
	s := "中国"
	t1 := []byte(s)
	fmt.Println(s)
	fmt.Println(t1)

}

func TestByte(t *testing.T) {
	i := int64(2)
	b2 := []byte(strconv.FormatInt(i, 2))
	b1 := []byte(strconv.FormatInt(i, 10))
	fmt.Println(b1, b2)

	t1 := 2
	tt := strconv.Itoa(t1)
	b3 := []byte(tt)
	fmt.Println(tt, b3)

}

func TestInt64(t *testing.T) {
	i := int64(2)
	b1 := make([]byte, 8)
	s := binary.PutVarint(b1, i)
	fmt.Println(s)
}

func TestInt(t *testing.T) {
	i := uint32(2)
	bs := make([]byte, 4)
	binary.LittleEndian.PutUint32(bs, i)
	fmt.Println(bs)
}

func TestUInt(t *testing.T) {

	s := byte(2)
	fmt.Println(s)
	// i := uint64(2)
	// b1 := make([]byte, 16)
	// binary.LittleEndian.PutUint64(b1, i)
	// fmt.Println(b1)
}

func TestIntToByte(t *testing.T) {
	v := int64(-2)
	if v == 0 {
		fmt.Println([]byte{0})
	} else {
		b := [8]byte{
			byte(0xff & v),
			byte(0xff & (v >> 8)),
			byte(0xff & (v >> 16)),
			byte(0xff & (v >> 24)),
			byte(0xff & (v >> 32)),
			byte(0xff & (v >> 40)),
			byte(0xff & (v >> 48)),
			byte(0xff & (v >> 56)),
		}
		l := len(b)
		for l > 0 {
			if b[l-1] > 0 {
				break
			}
			l = l - 1
		}
		fmt.Println(l)
		fmt.Println(b[:l])
	}

}

func TestUint64(t *testing.T) {
	// i := int64(-123456789)
	i := int64(2)

	d := binary.Size(i)

	fmt.Println(i, d)

	b := make([]byte, 8)
	binary.LittleEndian.PutUint64(b, uint64(i))

	fmt.Println(b)

	i = int64(binary.LittleEndian.Uint64(b))
	fmt.Println(i)
}

func TestUint32(t *testing.T) {
	// i := int64(-123456789)
	i := 258

	// b := []byte{}

	// b[0] = byte(v)
	// b[1] = byte(v >> 8)
	// b[2] = byte(v >> 16)
	// b[3] = byte(v >> 24)
	// fmt.Println(b)

	d := binary.Size(i)

	fmt.Println(i, d)

	b := make([]byte, 8)
	b1 := make([]byte, 8)
	binary.LittleEndian.PutUint32(b, uint32(i))
	binary.BigEndian.PutUint32(b1, uint32(i))

	fmt.Println(b)
	fmt.Println(b1)

	// i = int64(binary.LittleEndian.Uint64(b))
	// fmt.Println(i)
}

func TestUvarint(t *testing.T) {
	var i int64 = 123
	b := []byte(strconv.FormatInt(i, 2))
	x, n := binary.Uvarint(b)
	fmt.Println("x, n", x, n)
}

func TestVInt(t *testing.T) {
	var i int64 = 258
	str := fmt.Sprintf("%b", i)
	fmt.Println(str)
	// b := []byte(strconv.FormatInt(i, 2))
	// fmt.Println(strconv.FormatInt(i, 2))
	for (i & ^0x7F) != 0 {
		s1 := ((byte)((i & 0x7f) | 0x80))
		str := fmt.Sprintf("byte: %b, int: %v", s1, s1)
		fmt.Println("s1--", str)
		i >>= 7
	}
	s2 := byte(i)
	str = fmt.Sprintf("byte: %b, int: %v", s2, s2)
	fmt.Println("s2--", str)
}
