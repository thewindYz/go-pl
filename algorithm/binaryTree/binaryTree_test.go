package main

type node struct {
	val   int
	left  *node
	right *node
}

type btree struct {
	root *node
}
