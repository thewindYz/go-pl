package main

import "fmt"

// Tree 定义树节点
type Tree struct {
	Val    int
	Left   *Tree
	Right  *Tree
	IsRoot bool
}

var node9 = &Tree{
	Val: 9,
}
var node8 = &Tree{
	Val: 8,
}
var node7 = &Tree{
	Val: 7,
}
var node6 = &Tree{
	Val:   6,
	Left:  node8,
	Right: node9,
}
var node5 = &Tree{
	Val: 5,
}
var node4 = &Tree{
	Val: 4,
}
var node3 = &Tree{
	Val:   3,
	Left:  node6,
	Right: node7,
}
var node2 = &Tree{
	Val:   2,
	Left:  node4,
	Right: node5,
}
var root = &Tree{
	Val:    1,
	Left:   node2,
	Right:  node3,
	IsRoot: true,
}

var s []int

// preorder 前序遍历
func preorder(t *Tree) {
	if t == nil {
		return
	}
	s = append(s, t.Val)
	fmt.Println(t.Val)
	preorder(t.Left)
	preorder(t.Right)
}

func main() {
	fmt.Println(" 开始前序遍历 ")
	preorder(root)
	fmt.Println(" 遍历结束 ")
	fmt.Println(s)

}
