package gheap

import (
	"container/heap"
	"fmt"
	"testing"
)

type Item struct {
	value    string // value of item
	priority int    // the priority of the item
	index    int    // the index of the item in heap
}

type PriorityQuery []*Item

func (pq PriorityQuery) Len() int {
	return len(pq)
}

func (pq PriorityQuery) Less(i, j int) bool {
	return pq[i].priority > pq[j].priority
}

func (pq PriorityQuery) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index = i
	pq[j].index = j
}

func (pq *PriorityQuery) Push(x interface{}) {
	n := len(*pq)
	item := x.(*Item)
	item.index = n
	*pq = append(*pq, item)
}

func (pq *PriorityQuery) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	old[n-1] = nil
	item.index = -1
	*pq = old[0 : n-1]
	return item
}

func (pq *PriorityQuery) update(item *Item, value string, priority int) {
	item.value = value
	item.priority = priority
	heap.Fix(pq, item.index)
}

//
func TestPriorityQueue(t *testing.T) {
	// items := map[string]int{
	// 	"banana": 3,
	// 	"apple":  2,
	// 	"pear":   4,
	// }
	// pq := make(PriorityQuery, len(items))
	pq := make(PriorityQuery, 0)
	// i := 0
	// for value, priority := range items {
	// 	pq[i] = &Item{
	// 		value:    value,
	// 		priority: priority,
	// 		index:    i,
	// 	}
	// 	i++
	// }

	// heap.Init(&pq)

	item := &Item{
		value:    "orange",
		priority: 1,
	}
	heap.Push(&pq, item)
	pq.update(item, item.value, 2)

	for pq.Len() > 0 {
		item := heap.Pop(&pq).(*Item)
		fmt.Printf("%.2d : %s ", item.priority, item.value)
		fmt.Println()
	}
	fmt.Println("----")
}

func TestEOR(t *testing.T) {
	a := []int{1, 2, 3, 4, 5}
	b := []int{1, 2, 3, 4, 5, 4}
	var r int
	for _, i := range a {
		r = r ^ i
	}
	for _, j := range b {
		r = r ^ j
	}
	fmt.Println(r)
}

// {
// 	Index:"wksp_30de2cfa556411ebb5e3f697979255d9_logging-000001",
// 	Type:"_doc",
// 	Id:"7wCqSHcB2ferD8Qlp8bX",
// 	Version:0,
// 	Result:"",
// 	Shards:(*elastic.ShardsInfo)(nil),
// 	SeqNo:0,
// 	PrimaryTerm:0,
// 	Status:500,
// 	ForcedRefresh:false,
// 	Error:(*elastic.ErrorDetails)(0xc0001a6380),
// 	GetResult:(*elastic.GetResult)(nil)},
// 	err:&elastic.ErrorDetails{
// 		Type:"null_pointer_exception",
// 		Reason:"",
// 		ResourceType:"",
// 		ResourceId:"",
// 		Index:"",
// 		Phase:"",
// 		Grouped:false,
// 		CausedBy:map[string]interface {}(nil),
// 		RootCause:[]*elastic.ErrorDetails(nil),
// 		FailedShards:[]map[string]interface {}(nil),
// 		ScriptStack:[]string(nil),
// 		Script:"",
// 		Lang:"",
// 		Position:(*elastic.ScriptErrorPosition)(nil)
// 	}
// }

func TestStringBytes(t *testing.T) {
	s := "/etc/test.txt"
	b := []byte(s)
	fmt.Print(b)
}
