package gheap

import (
	"container/heap"
	"fmt"
	"testing"
)

func TestFloat(t *testing.T) {
	s := fmt.Sprintf("%f", 10.0002)
	fmt.Println(s)

	ss := float64(10)
	s1 := fmt.Sprintf("%.1f", ss)
	fmt.Println(s1)
}

// IntHeap an IntHeap is a min-heap of ints
type IntHeap []int

func (h IntHeap) Len() int {
	return len(h)
}

func (h IntHeap) Less(i, j int) bool {
	return h[i] < h[j]
}

func (h IntHeap) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}

func (h *IntHeap) Push(x interface{}) {

	*h = append(*h, x.(int))
}

func (h *IntHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

func TestIntHeap(t *testing.T) {
	h := &IntHeap{2, 1, 5}
	heap.Init(h)

	heap.Push(h, 3)
	fmt.Println("min value is ", (*h)[0])

	for h.Len() > 0 {
		fmt.Printf("%d ", heap.Pop(h))
	}
	fmt.Println("---")

}
